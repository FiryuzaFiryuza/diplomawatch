package com.example.macbook.diplomawatch;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;

import com.example.macbook.diplomawatch.dataBase.DataBase;
import com.example.macbook.diplomawatch.models.Program;
import com.example.macbook.diplomawatch.services.DBService;
import com.example.macbook.diplomawatch.services.NumberPickerService;

import java.io.IOException;

/**
 * Created by macbook on 17.11.15.
 */
public class ProgramDetails extends BaseActivity implements View.OnClickListener, NumberPicker.OnValueChangeListener {
    private EditText mEditName;

    //private Switch mSwitchUnit;
    private Button mBtnSec;
    private Button mBtnCardio;

    private TextView mTextAllDuration;
    private TextView mTextBreath;
    private TextView mTextOut;
    private TextView mTextHoldFirst;
    private TextView mTextHoldSecond;

    private Button mBttnAllDuration;
    private Button mBttnBreath;
    private Button mBttnOut;
    private Button mBttnHoldFirst;
    private Button mBttnHoldSecond;
    private Button mBttnTags;

    private long lessonId;
    private long programId;
    private Program mProgram = new Program();
    private Program mNewProgram = new Program();

    private DBService dbService = new DBService(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_details);

        initToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initializeControls();

        Bundle id = getIntent().getExtras();
        lessonId = id.getLong("new_program");
        if(lessonId <= 0)
            lessonId = id.getLong("lesson");
        programId = id.getLong("edit_program");

        if(programId > 0) {
            initialize();
        }
        else {
            setVisibility(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_save:
                save();
                return  true;
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_statistics:
                if(programId > 0) {
                    Intent intent = new Intent(ProgramDetails.this, ProgramChartsActivity.class);//ProgramLineChartActivity.class);
                    intent.putExtra("program", programId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);
                }
                return true;
            case R.id.action_search:
                Intent intent = new Intent(ProgramDetails.this, SearchExerciseActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.bttn_add_tags) {
            Intent intent = new Intent(ProgramDetails.this, AddSelectionsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("lesson", lessonId);
            intent.putExtra("program", programId);
            intent.putExtra("name", mProgram.getName());
            startActivity(intent);
        }
        else if(id == R.id.btn_sec) {
            mProgram.setUnit(true);
            mBtnSec.setTextColor(Color.parseColor("#FF4081"));
            mBtnSec.setBackgroundResource(R.drawable.tag_selected_button);
            mBtnCardio.setTextColor(Color.parseColor("#2ABCFF"));
            mBtnCardio.setBackgroundResource(R.drawable.tag_not_selected_button);
        }
        else if(id == R.id.btn_cardio) {
            mProgram.setUnit(false);
            mBtnCardio.setTextColor(Color.parseColor("#FF4081"));
            mBtnCardio.setBackgroundResource(R.drawable.tag_selected_button);
            mBtnSec.setTextColor(Color.parseColor("#2ABCFF"));
            mBtnSec.setBackgroundResource(R.drawable.tag_not_selected_button);
        }
        else {
            show(v.getId());
        }
    }

    private void save() {
        //TODO validation
        mProgram.setName(String.valueOf(mEditName.getEditableText()));
        mProgram.setBreath(mNewProgram.getBreath());
        mProgram.setOut(mNewProgram.getOut());
        mProgram.setHoldFirst(mNewProgram.getHoldFirst());
        mProgram.setHoldSecond(mNewProgram.getHoldSecond());
        //mProgram.setUnit(mSwitchUnit.isChecked());
        mProgram.setDuration(mNewProgram.getDuration());

        if(lessonId > 0) {
            mProgram.setLessonId(lessonId);
        }

        if(programId > 0) {
            dbService.updateProgram(mProgram);
        }
        else {
            dbService.addProgram(mProgram);
        }
    }

    private void show(final int id)
    {
        NumberPickerService numberPickerService = new NumberPickerService(this, "Длительность", 0, 100);

        final Dialog d = numberPickerService.getmDialog();

        Button mBttnOk = numberPickerService.getmBttnOk();
        Button mBttnCancel = numberPickerService.getmBttnCancel();

        final NumberPicker mNumPicker = numberPickerService.getmNumberPicker();
        mNumPicker.setDisplayedValues(generateFloatNumbers());
        mNumPicker.setOnValueChangedListener(this);

        mBttnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double value = (double) mNumPicker.getValue() / 2;

                switch (id) {
                    case R.id.tv_value_breath:
                        mTextBreath.setText(String.valueOf(value));
                        mNewProgram.setBreath(value);
                        break;
                    case R.id.bttn_breath:
                        mTextBreath.setText(String.valueOf(value));
                        mNewProgram.setBreath(value);
                        mTextBreath.setVisibility(View.VISIBLE);
                        mBttnBreath.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.tv_value_out:
                        mTextOut.setText(String.valueOf(value));
                        mNewProgram.setOut(value);
                        break;
                    case R.id.bttn_out:
                        mTextOut.setText(String.valueOf(value));
                        mNewProgram.setOut(value);
                        mTextOut.setVisibility(View.VISIBLE);
                        mBttnOut.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.tv_value_hold_first:
                        mTextHoldFirst.setText(String.valueOf(value));
                        mNewProgram.setHoldFirst(value);
                        break;
                    case R.id.bttn_hold_first:
                        mTextHoldFirst.setText(String.valueOf(value));
                        mNewProgram.setHoldFirst(value);
                        mTextHoldFirst.setVisibility(View.VISIBLE);
                        mBttnHoldFirst.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.tv_value_hold_second:
                        mTextHoldSecond.setText(String.valueOf(value));
                        mNewProgram.setHoldSecond(value);
                        break;
                    case R.id.bttn_hold_second:
                        mTextHoldSecond.setText(String.valueOf(value));
                        mNewProgram.setHoldSecond(value);
                        mTextHoldSecond.setVisibility(View.VISIBLE);
                        mBttnHoldSecond.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.tv_all_duration_value:
                        mTextAllDuration.setText(String.valueOf(value));
                        mNewProgram.setDuration(value);
                        break;
                    case R.id.bttn_all_duration:
                        mTextAllDuration.setText(String.valueOf(value));
                        mNewProgram.setDuration(value);
                        mTextAllDuration.setVisibility(View.VISIBLE);
                        mBttnAllDuration.setVisibility(View.INVISIBLE);
                        break;
                }
                d.dismiss();
            }
        });
        mBttnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();
    }


    private String[] generateFloatNumbers() {
        String[] numbers = new String[202];
        int n = 0;
        for (int i = 0; i < 201; i += 2) {
            numbers[i] = String.valueOf(n);
            numbers[i + 1] = String.valueOf(n + 0.5);
            n++;
        }

        return numbers;
    }

    private void initializeControls() {

        mEditName = (EditText) findViewById(R.id.et_program_name_detail);
        //mSwitchUnit = (Switch) findViewById(R.id.sw_unit);
        mBtnSec = (Button)findViewById(R.id.btn_sec);
        mBtnCardio = (Button)findViewById(R.id.btn_cardio);

        mBtnSec.setOnClickListener(this);
        mBtnCardio.setOnClickListener(this);

        mTextAllDuration = (TextView) findViewById(R.id.tv_all_duration_value);
        mTextBreath = (TextView) findViewById(R.id.tv_value_breath);
        mTextOut = (TextView) findViewById(R.id.tv_value_out);
        mTextHoldFirst = (TextView) findViewById(R.id.tv_value_hold_first);
        mTextHoldSecond = (TextView) findViewById(R.id.tv_value_hold_second);

        mBttnBreath = (Button) findViewById(R.id.bttn_breath);
        mTextBreath.setOnClickListener(this);
        mBttnBreath.setOnClickListener(this);

        mBttnOut = (Button) findViewById(R.id.bttn_out);
        mBttnOut.setOnClickListener(this);
        mTextOut.setOnClickListener(this);

        mBttnHoldFirst = (Button) findViewById(R.id.bttn_hold_first);
        mBttnHoldFirst.setOnClickListener(this);
        mTextHoldFirst.setOnClickListener(this);

        mBttnHoldSecond = (Button) findViewById(R.id.bttn_hold_second);
        mBttnHoldSecond.setOnClickListener(this);
        mTextHoldSecond.setOnClickListener(this);

        mBttnAllDuration = (Button) findViewById(R.id.bttn_all_duration);
        mBttnAllDuration.setOnClickListener(this);
        mTextAllDuration.setOnClickListener(this);

        mBttnTags = (Button)findViewById(R.id.bttn_add_tags);
        mBttnTags.setOnClickListener(this);
    }

    private void initialize()
    {
        mProgram = dbService.getProgramById(programId);

        mEditName.setText(mProgram.getName());

        if(mProgram.getUnit()) {
            mBtnSec.setTextColor(Color.parseColor("#FF4081"));
            mBtnSec.setBackgroundResource(R.drawable.tag_selected_button);
            //mSwitchUnit.setChecked(true);
        }
        else {
            mBtnCardio.setTextColor(Color.parseColor("#FF4081"));
            mBtnCardio.setBackgroundResource(R.drawable.tag_selected_button);
            //mSwitchUnit.setChecked(false);
        }

        setVisibility(false);

        setControlsFromReadyProgram();

        setProgramToNewProgram();
    }

    private void setVisibility(boolean visibility) {
        mBttnBreath.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        mTextBreath.setVisibility(visibility ? View.INVISIBLE : View.VISIBLE);

        mBttnOut.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        mTextOut.setVisibility(visibility ? View.INVISIBLE : View.VISIBLE);

        mBttnHoldFirst.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        mTextHoldFirst.setVisibility(visibility ? View.INVISIBLE : View.VISIBLE);

        mBttnHoldSecond.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        mTextHoldSecond.setVisibility(visibility ? View.INVISIBLE : View.VISIBLE);

        mBttnAllDuration.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        mTextAllDuration.setVisibility(visibility ? View.INVISIBLE : View.VISIBLE);
    }

    private void setControlsFromReadyProgram()
    {
        mTextBreath.setText(String.valueOf(mProgram.getBreath()));
        mTextOut.setText(String.valueOf(mProgram.getOut()));
        mTextHoldFirst.setText(String.valueOf(mProgram.getHoldFirst()));
        mTextHoldSecond.setText(String.valueOf(mProgram.getHoldSecond()));
        mTextAllDuration.setText(String.valueOf(mProgram.getDuration()));
    }

    private void setProgramToNewProgram()
    {
        mNewProgram.setName(mProgram.getName());
        mNewProgram.setUnit(mProgram.getUnit());
        mNewProgram.setBreath(mProgram.getBreath());
        mNewProgram.setOut(mProgram.getOut());
        mNewProgram.setHoldFirst(mProgram.getHoldFirst());
        mNewProgram.setHoldSecond(mProgram.getHoldSecond());
        mNewProgram.setDuration(mProgram.getDuration());
    }


    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }
}
