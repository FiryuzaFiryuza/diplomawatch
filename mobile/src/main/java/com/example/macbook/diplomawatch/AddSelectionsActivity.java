package com.example.macbook.diplomawatch;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.example.macbook.diplomawatch.adapters.ButtonAdapter;
import com.example.macbook.diplomawatch.models.Label;
import com.example.macbook.diplomawatch.models.SelectedLabel;
import com.example.macbook.diplomawatch.services.DBService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AddSelectionsActivity extends BaseActivity implements View.OnClickListener {
    private Button mButtonAdd;
    private EditText mEditTextNewSelection;
    private GridView mGridViewSelections;

    private ButtonAdapter buttonAdapter;

    private DBService dbService = new DBService(this);
    ArrayList<Label> labelList;
    ArrayList<SelectedLabel> selectedLabels;

    private long lessonId;
    private long programId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_selections);

        initToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Bundle id = getIntent().getExtras();
        lessonId = id.getLong("lesson");
        programId = id.getLong("program");

        setTitle(id.getString("name"));

        labelList = dbService.getLabels();
        selectedLabels = dbService.getSelectedLabels(lessonId, programId);

        mButtonAdd = (Button)findViewById(R.id.bttn_add_field);
        mButtonAdd.setOnClickListener(this);

        mEditTextNewSelection = (EditText) findViewById(R.id.et_new_selection);

        mGridViewSelections = (GridView) findViewById(R.id.gv_selection_exe);
        buttonAdapter = new ButtonAdapter(this, labelList, selectedLabels);
        mGridViewSelections.setAdapter(buttonAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.removeItem(R.id.action_statistics);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_save:
                save();
                return  true;
            case R.id.action_search:
                Intent intent = new Intent(AddSelectionsActivity.this, SearchExerciseActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        String item = mEditTextNewSelection.getText().toString();

        Label newLabel = new Label(item);
        dbService.addLabel(newLabel);
        labelList.add(newLabel);
        buttonAdapter.notifyDataSetChanged();

        Toast.makeText(AddSelectionsActivity.this, "Item Added SuccessFully", Toast.LENGTH_LONG).show();
    }

    private void save() {
        dbService.deleteSelectedLabels(lessonId, programId);

        ArrayList<SelectedLabel> newSelectedLabels = new ArrayList<>();
        for(int i = 0; i < SearchExerciseActivity.userSelections.size(); i++) {
            newSelectedLabels.add(new SelectedLabel(lessonId, programId, SearchExerciseActivity.userSelections.get(i), null,  null));
        }
        dbService.addSelectedLabels(newSelectedLabels);
    }
}
