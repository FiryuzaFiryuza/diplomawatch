package com.example.macbook.diplomawatch.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.macbook.diplomawatch.Helpers.CommonHelper;
import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.adapters.LessonChartRVAdapter;
import com.example.macbook.diplomawatch.models.PulseHistory;
import com.example.macbook.diplomawatch.models.PulseTimeChart;
import com.example.macbook.diplomawatch.services.DBService;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by macbook on 25.05.16.
 */
public class LessonMonthChartFragment extends Fragment {
    private ArrayList<PulseHistory> pulseHistories = new ArrayList<>();
    LinkedHashMap<String, ArrayList<PulseTimeChart>> pulseMap = new LinkedHashMap<>();;

    private DBService dbService;
    private long lessonId;

    View mView;
    Context mContext;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private LessonChartRVAdapter adapter;

    public LessonMonthChartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lessonId = getArguments().getLong("lessonId");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_lesson_chart, container, false);
        mContext = mView.getContext();

        dbService = new DBService(mContext);
        pulseHistories = dbService.getPulseHistoryByLessonId(lessonId);
        setPulseHistoryToPulseMap();


        mRecyclerView = (RecyclerView) mView.findViewById(R.id.rv_lesson_chart);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new LessonChartRVAdapter(pulseMap, mContext);
        mRecyclerView.setAdapter(adapter);

        // Inflate the layout for this fragment
        return mView;
    }

    private void setPulseHistoryToPulseMap() {
        int count = pulseHistories.size();
        String currentMonth = "";
        String currentDate = "";
        int pulseValues = 0;
        int pulseCount = 0;
        ArrayList<PulseTimeChart> list = new ArrayList<>();
        for(int i = 0; i < count; i++) {
            String[] dateTime = pulseHistories.get(i).getDateTime().split("/");
            String month = dateTime[0].split("\\.")[1];
            if(currentMonth.equals("")) {
                currentMonth = month;
                list = new ArrayList<>();
                currentDate = dateTime[0];
                pulseValues = pulseHistories.get(i).getPulse();
                pulseCount = 1;
            }
            else if(currentMonth.equals(month)) {
                if(currentDate.equals(dateTime[0])) {
                    pulseValues += pulseHistories.get(i).getPulse();
                    pulseCount++;
                }
                else {
                    list.add(new PulseTimeChart(currentDate, pulseValues/pulseCount, false));

                    currentDate = dateTime[0];
                    pulseValues = pulseHistories.get(i).getPulse();
                    pulseCount = 1;
                }
            }
            else {
                pulseMap.put(CommonHelper.getMonth(currentMonth), list);

                currentMonth = month;
                list = new ArrayList<>();
                currentDate = dateTime[0];
                pulseValues = pulseHistories.get(i).getPulse();
                pulseCount = 1;
            }
        }

        list.add(new PulseTimeChart(currentDate, pulseValues/pulseCount, false));
        pulseMap.put(String.valueOf(CommonHelper.getMonth(currentMonth)), list);

        pulseMap = CommonHelper.sortByDescending(pulseMap);
    }
}
