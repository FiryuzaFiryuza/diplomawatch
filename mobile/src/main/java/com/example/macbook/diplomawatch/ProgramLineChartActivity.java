package com.example.macbook.diplomawatch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.example.macbook.diplomawatch.Helpers.CommonHelper;
import com.example.macbook.diplomawatch.adapters.ProgramLineChartsRVAdapter;
import com.example.macbook.diplomawatch.models.PulseHistory;
import com.example.macbook.diplomawatch.models.PulseTimeChart;
import com.example.macbook.diplomawatch.services.DBService;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by macbook on 03.05.16.
 */
public class ProgramLineChartActivity extends BaseActivity {
    private ArrayList<PulseHistory> pulseHistories = new ArrayList<>();
    LinkedHashMap<String, ArrayList<PulseTimeChart>> pulseMap = new LinkedHashMap<String, ArrayList<PulseTimeChart>>();

    private DBService dbService = new DBService(this);
    private long programId;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgramLineChartsRVAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_linechart);

        initToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle id = getIntent().getExtras();
        programId = id.getLong("program");

        pulseHistories = dbService.getPulseHistoryByProgramId(programId);
        setPulseHistoryToPulseMap();

        /*
        ArrayList<PulseTimeChart> list = new ArrayList<>();
        list.add(new PulseTimeChart("10:30:56", 30));
        list.add(new PulseTimeChart("10:31:56", 70));
        list.add(new PulseTimeChart("10:33:54", 60));
        list.add(new PulseTimeChart("10:38:04", 85));

        pulseMap.put("10.05.2016", list);
        */

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_program_linechart);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new ProgramLineChartsRVAdapter(pulseMap, this);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setPulseHistoryToPulseMap() {
        int count = pulseHistories.size();
        for(int i = 0; i < count; i++) {
            String[] dateTime = pulseHistories.get(i).getDateTime().split("/");
            if(pulseMap.containsKey(dateTime[0])) {
                pulseMap.get(dateTime[0]).add(new PulseTimeChart(dateTime[1], pulseHistories.get(i).getPulse(), pulseHistories.get(i).isStart()));
            }
            else {
                ArrayList<PulseTimeChart> list = new ArrayList<>();
                list.add(new PulseTimeChart(dateTime[1], pulseHistories.get(i).getPulse(), pulseHistories.get(i).isStart()));
                pulseMap.put(dateTime[0], list);
            }
        }
        pulseMap = CommonHelper.sortByDescending(pulseMap);
    }
}
