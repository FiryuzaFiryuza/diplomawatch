package com.example.macbook.diplomawatch.animation;

import android.content.Context;

import com.example.macbook.diplomawatch.dataBase.DBFields;
import com.example.macbook.diplomawatch.models.Program;
import com.example.macbook.diplomawatch.models.PulseHistory;
import com.example.macbook.diplomawatch.models.SelectedLabel;
import com.example.macbook.diplomawatch.services.DBService;
import com.example.macbook.diplomawatch.services.DefineLabelService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by macbook on 10.06.16.
 */
public class AnimationService {
    private Context context;
    private DBService dbService;
    DefineLabelService defineLabelService;
    boolean isLessonForUpDownPulse;

    private HashMap<Long, ArrayList<String>> programPulseHistories = new HashMap<>();
    private long lastAdded = 0;
    private String lastAddedDateTime;

    public AnimationService(Context c) {
        context = c;
        dbService = new DBService(context);
    }

    public void setStatusForDefaultLabels(long lessonId, int sizeOfPrograms, ArrayList<Program> programList) {
        String name = dbService.getLessonById(lessonId).getName();
        isLessonForUpDownPulse = name.equals(DBFields.LESSON_UP_DOWN_PULSE);
        if(isLessonForUpDownPulse) {
            boolean isUpSetted = dbService.isUpPulseLabelSetted();
            boolean isDownSetted = dbService.isDownPulseLabelSetted();
            boolean isUpPulseHistorySetted = false;
            boolean isDownPulseHistorySetted = false;

            if(!isUpSetted || !isDownSetted) {
                long upId = 0;
                long downId = 0;
                for(int i = 0; i < sizeOfPrograms; i++) {
                    Program program = programList.get(i);
                    if(program.getName().equals(DBFields.PROGRAM_UP_PULSE)) {
                        upId = program.getId();
                    }
                    else if(program.getName().equals(DBFields.PROGRAM_DOWN_PULSE)) {
                        downId = program.getId();
                    }
                }
                if(!isUpSetted) {
                    isUpPulseHistorySetted = dbService.isPulseSettedForUpDownPulseLabels(lessonId, upId);
                }

                if(!isDownSetted) {
                    isDownPulseHistorySetted = dbService.isPulseSettedForUpDownPulseLabels(lessonId, downId);
                }

                ArrayList<SelectedLabel> selectedLabels = new ArrayList<>();
                if(isUpPulseHistorySetted) {
                    dbService.upPulseSetted();
                    SelectedLabel sl = new SelectedLabel();
                    sl.setLessonId(lessonId);
                    sl.setProgramId(upId);
                    sl.setLabelName(DBFields.UP_PULSE_LABEL);
                    sl.setStartDateTime(programPulseHistories.get(upId).get(0));
                    sl.setEndDateTime(programPulseHistories.get(upId).get(1));
                    selectedLabels.add(sl);
                }

                if(isDownPulseHistorySetted) {
                    dbService.downPulseSetted();
                    SelectedLabel sl = new SelectedLabel();
                    sl.setLessonId(lessonId);
                    sl.setProgramId(downId);
                    sl.setLabelName(DBFields.DOWN_PULSE_LABEL);
                    sl.setStartDateTime(programPulseHistories.get(downId).get(0));
                    sl.setEndDateTime(programPulseHistories.get(downId).get(1));
                    selectedLabels.add(sl);
                }

                if(selectedLabels.size() > 0) {
                    dbService.addSelectedLabels(selectedLabels);
                }
            }
        }
    }

    public boolean addNewPulseValue(boolean isStopped, boolean isStart, float prevPulse, long lessonId, int currentId, ArrayList<Program> programList) {
        if(!isStopped) {
            PulseHistory pulse = new PulseHistory();
            pulse.setPulse((int) prevPulse);
            pulse.setLessonId(lessonId);
            long id = programList.get(currentId).getId();
            pulse.setProgramId(id);

            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy/hh:mm:ss");
            lastAddedDateTime = df.format(Calendar.getInstance().getTime());

            pulse.setDateTime(lastAddedDateTime);

            if(isStart) {
                if(!programPulseHistories.containsKey(id)) {
                    if(lastAdded != 0) {
                        programPulseHistories.get(lastAdded).add(lastAddedDateTime);
                    }
                    ArrayList<String> date = new ArrayList<>();
                    date.add(lastAddedDateTime);
                    programPulseHistories.put(id, date);
                    lastAdded = id;
                }
            }
            pulse.setIsStart(isStart);
            isStart = false;

            dbService.addPulse(pulse);
        }

        return isStart;
    }

    public void setToProgramPulseHistories() {
        programPulseHistories.get(lastAdded).add(lastAddedDateTime);
    }

    public void defineLabel(long lessonId) {
        if(!isLessonForUpDownPulse) {
            defineLabelService = new DefineLabelService(context, lessonId);
            defineLabelService.setUpOrDownLabel(programPulseHistories);
        }
    }

    public boolean isWearUsed() {
        return lastAdded != 0;
    }

}
