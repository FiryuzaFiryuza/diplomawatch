package com.example.macbook.diplomawatch.services;

import android.content.Context;

import com.example.macbook.diplomawatch.Helpers.Algorithms;
import com.example.macbook.diplomawatch.dataBase.DBFields;
import com.example.macbook.diplomawatch.models.GCofDTW;
import com.example.macbook.diplomawatch.models.ProgramDTW;
import com.example.macbook.diplomawatch.models.PulseHistory;
import com.example.macbook.diplomawatch.models.SelectedLabel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 * Created by macbook on 09.06.16.
 */
public class DefineLabelService {
    private DBService dbService;
    private long lessonId;
    private HashMap<Long, ProgramDTW> GCs = new HashMap<>();
    private ArrayList<String> labelAsClass;

    public DefineLabelService(Context context, long lId) {
        dbService = new DBService(context);
        lessonId = lId;
        labelAsClass = new ArrayList<>();
        labelAsClass.add(DBFields.UP_PULSE_LABEL);
        labelAsClass.add(DBFields.DOWN_PULSE_LABEL);
    }

    public void setUpOrDownLabel(HashMap<Long, ArrayList<String>> programPulseHistories) {
        ArrayList<String> label = new ArrayList<>();

        label.add(DBFields.UP_PULSE_LABEL);
        ArrayList<SelectedLabel> marked = dbService.getByDefeniteLabel(label);
        setGCs(programPulseHistories, marked, DBFields.UP_PULSE_LABEL);

        label.clear();
        label.add(DBFields.DOWN_PULSE_LABEL);
        marked.clear();
        marked = dbService.getByDefeniteLabel(label);
        setGCs(programPulseHistories, marked, DBFields.DOWN_PULSE_LABEL);

        setLabel(labelAsClass, programPulseHistories);
    }

    private void setGCs(HashMap<Long, ArrayList<String>> programPulseHistories, ArrayList<SelectedLabel> withLabel, String label) {
        int count = programPulseHistories.size();
        int countWithLabels = withLabel.size();

        for(int i = 0; i < count; i++) {
            for(int j = 0; j < countWithLabels; j++) {
                if(withLabel.get(j).getProgramId() != 0) {
                    long programId = (long)programPulseHistories.keySet().toArray()[i];
                    ArrayList<PulseHistory> unknown = dbService.getPulseHistoryByLessonProgramDate(lessonId, programId,
                            programPulseHistories.get(programId).get(0),
                            programPulseHistories.get(programId).get(1));
                    ArrayList<PulseHistory> marked = dbService.getPulseHistoryByLessonProgramDate(withLabel.get(j).getLessonId(),
                            withLabel.get(j).getProgramId(),
                            withLabel.get(j).getStartDateTime(),
                            withLabel.get(j).getEndDateTime());
                    prepareAndRunDTW(unknown, marked, programId, label);
                }
            }
        }
    }

    private void setLabel(ArrayList<String> labels, HashMap<Long, ArrayList<String>> programPulseHistories) {
        ArrayList<SelectedLabel> chosenLabels = new ArrayList<>();
        int count = GCs.size();
        String label = "";
        double averageMin = Double.MAX_VALUE;
        for(int i = 0; i < count; i++) {
            long key = (long)GCs.keySet().toArray()[i];
            ProgramDTW values = GCs.get(key);
            for(int j = 0; j < labels.size(); j++) {
                String takenLabel = labels.get(j);
                int allCount = values.getLabelCount().get(takenLabel);
                double min = values.getLabelSumGC().get((takenLabel));
                double av = min / allCount;
                if(av < averageMin) {
                    averageMin = av;
                    label = takenLabel;
                }
            }
            SelectedLabel s = new SelectedLabel();
            s.setProgramId(key);
            s.setLessonId(lessonId);
            s.setStartDateTime(programPulseHistories.get(key).get(0));
            s.setStartDateTime(programPulseHistories.get(key).get(1));
            s.setLabelName(label);
            chosenLabels.add(s);
        }
        updateAndSetLabel(chosenLabels);
    }

    private void prepareAndRunDTW(ArrayList<PulseHistory> unknown, ArrayList<PulseHistory> marked, long programId, String label) {
        int countA = unknown.size();
        int countB = marked.size();

        int[] a = new int[countA];
        int[] b = new int[countB];

        for(int i = 0; i < countA; i++) {
            a[i] = unknown.get(i).getPulse();
        }
        for (int i = 0; i < countB; i++) {
            b[i] = marked.get(i).getPulse();
        }

        Stack<Double> w = Algorithms.dtw(a, b);
        double min = Algorithms.getGC(w);
        GCofDTW obj = new GCofDTW(min, label);
        if(GCs.containsKey(programId)) {
            if(GCs.get(programId).getLabelCount().containsKey(label)) {
                int c = GCs.get(programId).getLabelCount().get(label);
                GCs.get(programId).getLabelCount().values().toArray()[0] = ++c;
                double m = GCs.get(programId).getLabelSumGC().get(label);
                GCs.get(programId).getLabelSumGC().values().toArray()[0] = min + m;
            }
            else {
                GCs.get(programId).getLabelCount().put(label, 1);
                GCs.get(programId).getLabelSumGC().put(label, min);
            }
            GCs.get(programId).getGCs().add(obj);
        }
        else {
            ArrayList<GCofDTW> list = new ArrayList<>();
            list.add(obj);

            ProgramDTW prDTW = new ProgramDTW();
            prDTW.getLabelCount().put(label, 1);
            prDTW.getLabelSumGC().put(label, min);
            prDTW.setGCs(list);

            GCs.put(programId, prDTW);
        }
    }

    private void updateAndSetLabel(ArrayList<SelectedLabel> chosenLabels) {
        for(int i = 0; i < labelAsClass.size(); i++) {
            for(int j = 0; j < chosenLabels.size(); j++) {
                dbService.deleteByLabel(chosenLabels.get(j).getLessonId(), chosenLabels.get(j).getProgramId(), labelAsClass.get(i));
            }
        }

        dbService.addSelectedLabels(chosenLabels);
    }
}
