package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 17.11.15.
 */
public class PersistentObject {
    private long Id;

    public PersistentObject() {
    }

    public PersistentObject(long id) {
        Id = id;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        this.Id = id;
    }
}
