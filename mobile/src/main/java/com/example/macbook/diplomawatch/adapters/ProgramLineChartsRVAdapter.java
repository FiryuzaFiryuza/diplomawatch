package com.example.macbook.diplomawatch.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.models.PulseTimeChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.TreeMap;

/**
 * Created by macbook on 10.05.16.
 */
public class ProgramLineChartsRVAdapter extends RecyclerView.Adapter<ProgramLineChartsRVAdapter.LineChartViewHolder> {
    private LinkedHashMap<String, ArrayList<PulseTimeChart>> pulseHistoryMap;
    private Context mContext;

    public class LineChartViewHolder extends RecyclerView.ViewHolder {
        LineChart mChart;
        TextView mTextViewDate;

        public LineChartViewHolder(View itemView) {
            super(itemView);

            mChart = (LineChart) itemView.findViewById(R.id.chart1);
            mTextViewDate = (TextView) itemView.findViewById(R.id.tv_date_chart);
        }
    }

    private LineChartViewHolder lineChartViewHolder;

    public ProgramLineChartsRVAdapter(LinkedHashMap<String, ArrayList<PulseTimeChart>> list, Context context) {
        pulseHistoryMap = list;
        this.mContext = context;
    }

    @Override
    public LineChartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context context = parent.getContext();

        View view = LayoutInflater.from(context)
                .inflate(R.layout.activity_linechart, parent, false);

        lineChartViewHolder = new LineChartViewHolder(view);
        return lineChartViewHolder;
    }

    @Override
    public void onBindViewHolder(LineChartViewHolder holder, int position) {
        drawChart(holder, position);
    }

    @Override
    public int getItemCount() {
        return pulseHistoryMap.size();
    }

    private void drawChart(LineChartViewHolder holder, int position) {
        // no description text
        holder.mChart.setDescription("");
        holder.mChart.setNoDataTextDescription("You need to provide data for the chart.");

        // enable touch gestures
        holder.mChart.setTouchEnabled(true);

        holder.mChart.setDragDecelerationFrictionCoef(0.9f);

        // enable scaling and dragging
        holder.mChart.setDragEnabled(true);
        holder.mChart.setScaleEnabled(true);
        holder.mChart.setDrawGridBackground(false);
        holder.mChart.setHighlightPerDragEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        holder.mChart.setPinchZoom(true);

        // set an alternative background color
        holder.mChart.setBackgroundColor(Color.WHITE);//(Color.LTGRAY);

        // add data
        //setData(20, 30, holder.mChart);
        setData(position, holder.mChart, holder.mTextViewDate);

        holder.mChart.animateX(2500);

        //Typeface tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        // get the legend (only possible after setting data)
        Legend l = holder.mChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(Legend.LegendForm.LINE);
        //l.setTypeface(tf);
        l.setTextSize(11f);
        l.setTextColor(Color.BLACK);//(Color.WHITE);
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
//        l.setYOffset(11f);

        XAxis xAxis = holder.mChart.getXAxis();
        //xAxis.setTypeface(tf);
        xAxis.setTextSize(12f);
        xAxis.setTextColor(Color.BLACK);//(Color.WHITE);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setSpaceBetweenLabels(1);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = holder.mChart.getAxisLeft();
        //leftAxis.setTypeface(tf);
        leftAxis.setTextColor(Color.BLACK);//(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMaxValue(200f);
        leftAxis.setAxisMinValue(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        holder.mChart.getAxisRight().setEnabled(false); // no right axis
    }

    private void setData(int position, LineChart mChart, TextView mTextViewDate) {
        ArrayList<String> xVals = new ArrayList<String>();
        String date = (String) pulseHistoryMap.keySet().toArray()[position];
        ArrayList<PulseTimeChart> pulseHistory = pulseHistoryMap.get(date);

        //set Date to TextView
        mTextViewDate.setText(date);

        int count = pulseHistory.size();
        for (int i = 0; i < count; i++) {
            xVals.add(pulseHistory.get(i).getTime() + "");
        }

        ArrayList<ArrayList<Entry>> allYVals = new ArrayList<ArrayList<Entry>>();
        ArrayList<Entry> yVals = new ArrayList<>();
        for(int i = 0; i < count; i++) {
            if(pulseHistory.get(i).isStart()) {
                if(!yVals.isEmpty()) {
                    allYVals.add(yVals);
                }
                yVals = new ArrayList<Entry>();
                yVals.add(new Entry(pulseHistory.get(i).getPulseValue(), i));
            }
            else {
                yVals.add(new Entry(pulseHistory.get(i).getPulseValue(), i));
            }
        }
        if(!yVals.isEmpty()) {
            allYVals.add(yVals);
        }

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();

        int allYCount = allYVals.size();
        for(int i = 0; i < allYCount; i++) {
            LineDataSet set = new LineDataSet(allYVals.get(i),"");

            set.setAxisDependency(YAxis.AxisDependency.LEFT);
            set.setColor(ColorTemplate.getHoloBlue());
            set.setCircleColor(Color.parseColor("#FF4081"));//(Color.WHITE);
            set.setLineWidth(2f);
            set.setCircleRadius(3f);
            set.setFillAlpha(65);
            set.setFillColor(ColorTemplate.getHoloBlue());
            set.setHighLightColor(Color.rgb(244, 117, 117));
            set.setDrawCircleHole(false);

            dataSets.add(set);
        }

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);
        data.setValueTextColor(Color.WHITE);
        data.setValueTextSize(9f);

        // set data
        mChart.setData(data);
    }
}
