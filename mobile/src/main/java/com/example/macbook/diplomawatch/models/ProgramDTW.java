package com.example.macbook.diplomawatch.models;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by macbook on 10.06.16.
 */
public class ProgramDTW {
    private long ProgramId;
    private HashMap<String, Integer> labelCount;
    private HashMap<String, Double> labelSumGC;
    private ArrayList<GCofDTW> GCs;

    public ProgramDTW(long programId, HashMap<String, Integer> labelCount, HashMap<String, Double> labelSumGC, ArrayList<GCofDTW> gCs) {
        ProgramId = programId;
        this.labelCount = labelCount;
        this.labelSumGC = labelSumGC;
        GCs = gCs;
    }

    public ProgramDTW() {
        labelCount = new HashMap<>();
        labelSumGC = new HashMap<>();
        GCs = new ArrayList<>();
    }

    public HashMap<String, Integer> getLabelCount() {
        return labelCount;
    }

    public void setLabelCount(HashMap<String, Integer> labelCount) {
        this.labelCount = labelCount;
    }

    public HashMap<String, Double> getLabelSumGC() {
        return labelSumGC;
    }

    public void setLabelSumGC(HashMap<String, Double> labelSumGC) {
        this.labelSumGC = labelSumGC;
    }

    public ArrayList<GCofDTW> getGCs() {
        return GCs;
    }

    public void setGCs(ArrayList<GCofDTW> GCs) {
        this.GCs = GCs;
    }
}
