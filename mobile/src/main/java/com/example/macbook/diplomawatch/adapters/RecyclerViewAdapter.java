package com.example.macbook.diplomawatch.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.macbook.diplomawatch.LessonDetails;
import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.animation.ColorizeActivity;
import com.example.macbook.diplomawatch.models.Lesson;
import com.example.macbook.diplomawatch.models.NameValuePair;
import com.example.macbook.diplomawatch.services.NumberPickerService;

import java.util.ArrayList;

/**
 * Created by macbook on 16.11.15.
 */
public class RecyclerViewAdapter extends RecyclerView
        .Adapter<RecyclerViewAdapter.LessonViewHolder> implements NumberPicker.OnValueChangeListener {

    private ArrayList<Lesson> mLessons;
    private double userPulse = 80;
    private Context context;

    public RecyclerViewAdapter(ArrayList<Lesson> list, Context context) {
        mLessons = list;
        this.context = context;
    }

    @Override
    public LessonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context context = parent.getContext();

        View view = LayoutInflater.from(context)
                .inflate(R.layout.lesson_cards_layout, parent, false);

        LessonViewHolder lessonViewHolder = new LessonViewHolder(view);
        return lessonViewHolder;
    }

    @Override
    public void onBindViewHolder(LessonViewHolder holder, final int position) {
        holder.name.setText(mLessons.get(position).getName());
        holder.description.setText(mLessons.get(position).getDescription());
        holder.bttnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show(position);
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, LessonDetails.class);
                intent.putExtra("lesson", mLessons.get(position).getId());
                Log.d("Click","Click");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLessons.size();
    }

    public void addItem(Lesson lesson, int index) {
        mLessons.add(index, lesson);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mLessons.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    private void show(final int position)
    {
        NumberPickerService numberPickerService = new NumberPickerService(context, "Введите пульс", 0, 150);

        final Dialog d = numberPickerService.getmDialog();

        Button mBttnOk = numberPickerService.getmBttnOk();
        Button mBttnCancel = numberPickerService.getmBttnCancel();

        final NumberPicker mNumPicker = numberPickerService.getmNumberPicker();
        mNumPicker.setDisplayedValues(generateFloatNumbers());
        mNumPicker.setOnValueChangedListener(this);

        mBttnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPulse = (double) mNumPicker.getValue() + 50;

                ArrayList<NameValuePair> listForSend = new ArrayList<NameValuePair>();
                listForSend.add(new NameValuePair("lesson", String.valueOf(mLessons.get(position).getId())));
                listForSend.add(new NameValuePair("pulse", String.valueOf(userPulse)));

                Bundle extra = new Bundle();
                extra.putSerializable("objects", listForSend);

                Context context = v.getContext();
                Intent intent = new Intent(context, ColorizeActivity.class);
                intent.putExtra("start", extra);
                context.startActivity(intent);

                d.dismiss();
            }
        });
        mBttnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();
    }

    private String[] generateFloatNumbers() {
        String[] numbers = new String[251];
        for (int i = 0; i < 251; i ++) {
            numbers[i] = String.valueOf(i + 50);
        }

        return numbers;
    }

    public class LessonViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView photo;
        TextView name;
        TextView description;
        Button bttnStart;

        public LessonViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            photo = (ImageView) itemView.findViewById(R.id.iv_photo);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            description = (TextView) itemView.findViewById(R.id.tv_description);
            bttnStart = (Button) itemView.findViewById(R.id.bttn_start_lesson);
        }
    }
}
