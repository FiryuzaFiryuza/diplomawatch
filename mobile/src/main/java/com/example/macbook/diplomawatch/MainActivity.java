package com.example.macbook.diplomawatch;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.macbook.diplomawatch.adapters.LessonSwipeAdapter;
import com.example.macbook.diplomawatch.models.Lesson;
import com.example.macbook.diplomawatch.services.DBService;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.marshalchen.ultimaterecyclerview.swipe.SwipeItemManagerInterface;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {
    private LessonSwipeAdapter adapter;
    private UltimateRecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private TextView mTextNoLessons;
    private FloatingActionButton mBttnAdd;

    private DBService dbService = new DBService(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();
        initControls();

        final ArrayList<Lesson> lessons = dbService.getLessons();

        mRecyclerView = (UltimateRecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new LessonSwipeAdapter(lessons, this);
        adapter.setMode(SwipeItemManagerInterface.Mode.Single);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.removeItem(R.id.action_save);
        menu.removeItem(R.id.action_statistics);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                Intent intent = new Intent(MainActivity.this, SearchExerciseActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initControls() {
        mBttnAdd = (FloatingActionButton) findViewById(R.id.bttn_lesson_add);
        mBttnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LessonDetails.class);
                intent.putExtra("lesson", -1);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
            }
        });
    }

    public void onDismiss(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int position) {
        //TODO delete the actual item in your data source
        adapter.notifyItemRemoved(position);
    }
}
