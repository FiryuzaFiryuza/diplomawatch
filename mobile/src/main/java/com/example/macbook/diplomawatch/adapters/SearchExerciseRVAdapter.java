package com.example.macbook.diplomawatch.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.macbook.diplomawatch.LessonDetails;
import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.models.LessonProgram;

import java.util.ArrayList;

/**
 * Created by macbook on 30.05.16.
 */
public class SearchExerciseRVAdapter extends RecyclerView
        .Adapter<SearchExerciseRVAdapter.SearchExerciseViewHolder> {
    private ArrayList<LessonProgram> mExercises;
    private SearchExerciseViewHolder exerciseViewHolder;
    private Context context;

    public SearchExerciseRVAdapter(ArrayList<LessonProgram> list, Context context) {
        mExercises = list;
        this.context = context;
    }
    @Override
    public SearchExerciseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context context = parent.getContext();

        View view = LayoutInflater.from(context)
                .inflate(R.layout.found_lesson_cards_layout, parent, false);

        exerciseViewHolder = new SearchExerciseViewHolder(view);
        return exerciseViewHolder;
    }

    @Override
    public void onBindViewHolder(SearchExerciseViewHolder holder, final int position) {
        holder.mLessonName.setText(mExercises.get(position).getLesson().getName());
        if(mExercises.get(position).getProgram() != null) {
            holder.mProgramName.setText(mExercises.get(position).getProgram().getName());
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LessonDetails.class);
                intent.putExtra("lesson", mExercises.get(position).getLesson().getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mExercises.size();
    }

    public void notify(ArrayList<LessonProgram> list){
        if (mExercises != null) {
            mExercises.clear();
            mExercises.addAll(list);
        }
        else {
            mExercises = list;
        }
        notifyDataSetChanged();
    }

    public class SearchExerciseViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView mLessonName;
        TextView mProgramName;

        public SearchExerciseViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
            mLessonName = (TextView) itemView.findViewById(R.id.tv_lesson_name);
            mProgramName = (TextView) itemView.findViewById(R.id.tv_program_name);
        }
    }
}
