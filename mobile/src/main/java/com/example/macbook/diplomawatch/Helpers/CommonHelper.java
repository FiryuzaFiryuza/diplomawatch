package com.example.macbook.diplomawatch.Helpers;

import com.example.macbook.diplomawatch.models.PulseTimeChart;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by macbook on 25.05.16.
 */
public class CommonHelper {
    public static LinkedHashMap<String, ArrayList<PulseTimeChart>> sortByDescending(LinkedHashMap<String, ArrayList<PulseTimeChart>> pulseMap) {
        LinkedHashMap<String, ArrayList<PulseTimeChart>> pulseMapSorted = new LinkedHashMap<String, ArrayList<PulseTimeChart>>();
        int count = pulseMap.size();

        for (int i = count - 1; i >= 0; i--) {
            String key = (String) pulseMap.keySet().toArray()[i];
            ArrayList<PulseTimeChart> list = pulseMap.get(key);
            pulseMapSorted.put(key, list);
        }
        return pulseMapSorted;
    }

    public static String getMonth(String strNumber) {
        switch(strNumber) {
            case "01":
                return "Январь";
            case "02":
                return "Февраль";
            case "03":
                return "Март";
            case "04":
                return "Апрель";
            case "05":
                return "Май";
            case "06":
                return "Июнь";
            case "07":
                return "Июль";
            case "08":
                return "Август";
            case "09":
                return "Сентябрь";
            case "10":
                return "Октябрь";
            case "11":
                return "Ноябрь";
            case "12":
                return "Декабрь";
        }
        return "";
    }
}
