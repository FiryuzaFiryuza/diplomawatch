package com.example.macbook.diplomawatch.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.macbook.diplomawatch.Helpers.CommonHelper;
import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.adapters.LessonChartRVAdapter;
import com.example.macbook.diplomawatch.adapters.ProgramLineChartsRVAdapter;
import com.example.macbook.diplomawatch.models.PulseHistory;
import com.example.macbook.diplomawatch.models.PulseTimeChart;
import com.example.macbook.diplomawatch.services.DBService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by macbook on 24.05.16.
 */
public class LessonDayChartFragment extends Fragment {
    private ArrayList<PulseHistory> pulseHistories = new ArrayList<>();
    LinkedHashMap<String, ArrayList<PulseTimeChart>> pulseMap = new LinkedHashMap<>();

    private DBService dbService;
    private long lessonId;

    View mView;
    Context mContext;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgramLineChartsRVAdapter adapter;

    public LessonDayChartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lessonId = getArguments().getLong("lessonId");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_lesson_chart, container, false);
        mContext = mView.getContext();

        dbService = new DBService(mContext);
        pulseHistories = dbService.getPulseHistoryByLessonId(lessonId);
        setPulseHistoryToPulseMap();


        mRecyclerView = (RecyclerView) mView.findViewById(R.id.rv_lesson_chart);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new ProgramLineChartsRVAdapter(pulseMap, mContext);
        mRecyclerView.setAdapter(adapter);

        // Inflate the layout for this fragment
        return mView;
    }

    private void setPulseHistoryToPulseMap() {
        int count = pulseHistories.size();
        for(int i = 0; i < count; i++) {
            String[] dateTime = pulseHistories.get(i).getDateTime().split("/");
            if(pulseMap.containsKey(dateTime[0])) {
                pulseMap.get(dateTime[0]).add(new PulseTimeChart(dateTime[1], pulseHistories.get(i).getPulse(), pulseHistories.get(i).isStart()));
            }
            else {
                ArrayList<PulseTimeChart> list = new ArrayList<>();
                list.add(new PulseTimeChart(dateTime[1], pulseHistories.get(i).getPulse(), pulseHistories.get(i).isStart()));
                pulseMap.put(dateTime[0], list);
            }
        }
        pulseMap = CommonHelper.sortByDescending(pulseMap);
    }
}
