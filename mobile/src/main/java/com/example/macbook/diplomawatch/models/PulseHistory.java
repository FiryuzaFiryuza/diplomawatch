package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 26.04.16.
 */
public class PulseHistory extends PersistentObject {

    private long LessonId;

    private long ProgramId;

    private String DateTime;

    private int Pulse;

    // true(1) - если это начало, false(0) - если это не начало упражнения
    private boolean isStart;

    public long getLessonId() {
        return LessonId;
    }

    public void setLessonId(long lessonId) {
        LessonId = lessonId;
    }

    public long getProgramId() {
        return ProgramId;
    }

    public void setProgramId(long programId) {
        ProgramId = programId;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public int getPulse() {
        return Pulse;
    }

    public void setPulse(int pulse) {
        Pulse = pulse;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setIsStart(boolean isStart) {
        this.isStart = isStart;
    }
}
