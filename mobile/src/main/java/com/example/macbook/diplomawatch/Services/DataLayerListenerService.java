package com.example.macbook.diplomawatch.services;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by macbook on 13.02.16.
 */
public class DataLayerListenerService extends WearableListenerService {

    private static final String LOG_TAG = "WearableListener";
    public static final String HEARTBEAT = "HEARTBEAT";
    private static final String animationStatePath = "wear/exercise/animation";

    private static Handler heartBeatHandler;
    private static Handler animationStateHandler;
    private static int currentValue=0;
    private static String animationState = "NONE";

    public static Handler getHeartBeatHandler() {
        return heartBeatHandler;
    }

    public static void setHeartBeatHandler(Handler heartBeatHandler) {
        DataLayerListenerService.heartBeatHandler = heartBeatHandler;
        // send current value as initial value.
        if(heartBeatHandler !=null)
            heartBeatHandler.sendEmptyMessage(currentValue);
    }

    public static Handler getAnimationStateHandler() {
        return animationStateHandler;
    }

    public static void setAnimationStateHandler(Handler animationStateHandler) {
        DataLayerListenerService.animationStateHandler = animationStateHandler;
        // send current value as initial value.
        if(animationStateHandler != null) {
            Message msg = animationStateHandler.obtainMessage();
            msg.obj = animationState;
            animationStateHandler.sendMessage(msg);
        }
    }

    @Override
    public void onPeerConnected(Node peer) {
        super.onPeerConnected(peer);

        String id = peer.getId();
        String name = peer.getDisplayName();

        Log.d(LOG_TAG, "Connected peer name & ID: " + name + "|" + id);
    }
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        String path = messageEvent.getPath();

        Log.d(LOG_TAG, "received a message from wear: " + path);
        if(path.equalsIgnoreCase(animationStatePath)) {
            animationState = new String(messageEvent.getData());
            if(animationStateHandler != null) {
                Message msg = animationStateHandler.obtainMessage();
                msg.obj = animationState;
                animationStateHandler.sendMessage(msg);
                Log.d(LOG_TAG, "Message received on watch is: " + animationState);
            }
        }
        else {
            // save the new heartbeat value
            currentValue = Integer.parseInt(path);
            if(heartBeatHandler != null) {
                // if a heartBeatHandler is registered, send the value as new message
                heartBeatHandler.sendEmptyMessage(currentValue);
            }
        }
    }


}