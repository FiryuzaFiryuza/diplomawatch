package com.example.macbook.diplomawatch.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.macbook.diplomawatch.ProgramDetails;
import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.models.Program;
import com.example.macbook.diplomawatch.services.DBService;
import com.marshalchen.ultimaterecyclerview.SwipeableUltimateViewAdapter;
import com.marshalchen.ultimaterecyclerview.URLogs;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.swipe.SwipeLayout;

import java.util.ArrayList;

/**
 * Created by macbook on 30.03.16.
 */
public class ProgramSwipeAdapter extends SwipeableUltimateViewAdapter {

    private ArrayList<Program> mProgramList;
    private Context context;

    private DBService dbService;

    public ProgramSwipeAdapter(ArrayList<Program> list, Context context) {
        mProgramList = list;
        this.context = context;
        dbService = new DBService(context);
    }

    @Override
    public UltimateRecyclerviewViewHolder getViewHolder(View view) {
        return new UltimateRecyclerviewViewHolder(view);
    }

    @Override
    public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup parent) {
        final Context context = parent.getContext();

        View view = LayoutInflater.from(context)
                .inflate(R.layout.program_cards_layout, parent, false);

        final ProgramViewHolder programViewHolder = new ProgramViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                URLogs.d("click");
            }
        });
        SwipeLayout swipeLayout = programViewHolder.swipeLayout;
        swipeLayout.setDragEdge(SwipeLayout.DragEdge.Right);
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        swipeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, ProgramDetails.class);
                intent.putExtra("edit_program", mProgramList.get(programViewHolder.getAdapterPosition()).getId());
                intent.putExtra("lesson", mProgramList.get(programViewHolder.getAdapterPosition()).getLessonId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                return true;
            }
        });

        programViewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(programViewHolder.getPosition());
                Toast.makeText(v.getContext(), "Deleted ", Toast.LENGTH_SHORT).show();
            }
        });

        return programViewHolder;
    }

    @Override
    public int getItemCount() {
        return mProgramList.size();
    }

    @Override
    public int getAdapterItemCount() {
        return mProgramList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    public void add(Program program) {
        insert(program, mProgramList.size());
    }

    public void insert(Program program, int index) {
        closeAllExcept(null);

        mProgramList.add(index, program);
        notifyItemInserted(index);
    }

    public void remove(int position) {
        long id = mProgramList.get(position).getId();
        dbService.deleteProgram(id);

        mProgramList.remove(position);
        closeItem(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onBindViewHolder(final UltimateRecyclerviewViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        ((ProgramViewHolder)holder).name.setText(mProgramList.get(position).getName());
        String unit = this.mProgramList.get(position).getUnit() ? "сек" : "кардио";
        ((ProgramViewHolder)holder).duration.setText(String.valueOf(
                this.mProgramList.get(position).getDuration()) + " " +
                unit);
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    public class ProgramViewHolder extends UltimateRecyclerviewViewHolder {
        TextView name;
        TextView duration;

        public Button deleteButton;

        public ProgramViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_program_name);
            duration = (TextView) itemView.findViewById(R.id.tv_program_duration);

            deleteButton = (Button) itemView.findViewById(R.id.delete);
        }
    }
}
