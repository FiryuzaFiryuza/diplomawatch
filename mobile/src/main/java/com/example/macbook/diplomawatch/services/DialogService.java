package com.example.macbook.diplomawatch.services;

import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.TextView;

import com.example.macbook.diplomawatch.R;

/**
 * Created by macbook on 08.06.16.
 */
public class DialogService {

    private Dialog mDialog;
    private Button mBttnOk;
    private Button mBttnCancel;
    private TextView mTextVewMessage;
    private Context context;
    private String title;
    private String message;

    public DialogService(Context context, String title, String message) {
        this.context = context;
        this.title = title;
        this.message = message;

        init();
    }

    private void init() {
        mDialog = new Dialog(context);
        mDialog.setTitle(title);
        mDialog.setContentView(R.layout.activity_set_default_labels);

        mBttnOk= (Button) mDialog.findViewById(R.id.button1);
        mBttnCancel = (Button) mDialog.findViewById(R.id.button2);
        mTextVewMessage = (TextView) mDialog.findViewById(R.id.tv_message);

        mTextVewMessage.setText(message);
    }

    public Dialog getmDialog() {
        return mDialog;
    }

    public Button getmBttnOk() {
        return mBttnOk;
    }

    public Button getmBttnCancel() {
        return mBttnCancel;
    }
}
