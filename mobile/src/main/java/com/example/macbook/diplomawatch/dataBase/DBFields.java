package com.example.macbook.diplomawatch.dataBase;

/**
 * Created by macbook on 01.12.15.
 */
public class DBFields {
    // tables in db
    public static final String TABLE_LESSON = "lesson";
    public static final String TABLE_PROGRAM = "program";
    public static final String TABLE_PULSE_HISTORY = "pulse_history";
    public static final String TABLE_LABEL = "label";
    public static final String TABLE_SELECTED_LABEL = "selected_label";
    public static final String TABLE_CHECK_DEFAULT_LABELS = "check_default_labels";

    // common fileds in tables
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_DURATION = "duration";

    // other fileds of all tables
    public static final String KEY_IS_SYSTEM_LESSON = "is_system_lesson";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_BREATH = "breath";
    public static final String KEY_OUT = "out";
    public static final String KEY_HOLD_FIRST= "hold_first";
    public static final String KEY_HOLD_SECOND= "hold_second";
    public static final String KEY_UNIT= "unit";

    public static final String KEY_LESSON_ID = "lesson_id";
    public static final String KEY_PROGRAM_ID = "program_id";

    public static final String KEY_DATETIME = "date_time";
    public static final String KEY_PULSE = "pulse";
    public static final String KEY_IS_START = "is_start";

    public static final String KEY_LABEL_NAME = "label_name";
    public static final String KEY_START_DATETIME = "start_date_time";
    public static final String KEY_END_DATETIME = "end_date_time";

    public static final String KEY_IS_SETTED = "is_setted";

    public static final String LESSON_UP_DOWN_PULSE = "Установление значений повышенного и пониженного пульсов";
    public static final String PROGRAM_UP_PULSE = "Повысить пульс";
    public static final String PROGRAM_DOWN_PULSE = "Понизить пульс";
    public static final String UP_PULSE_LABEL = "повышение пульса";
    public static final String DOWN_PULSE_LABEL = "понижение пульса";
}
