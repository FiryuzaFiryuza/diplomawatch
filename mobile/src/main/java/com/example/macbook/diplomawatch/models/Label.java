package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 29.05.16.
 */
public class Label extends PersistentObject{
    private String name;

    public Label() {}

    public Label(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
