package com.example.macbook.diplomawatch;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by macbook on 26.11.15.
 */
public class BaseActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    public void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if(mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }
}
