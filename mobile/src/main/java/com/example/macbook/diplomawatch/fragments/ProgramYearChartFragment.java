package com.example.macbook.diplomawatch.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.macbook.diplomawatch.Helpers.CommonHelper;
import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.adapters.LessonChartRVAdapter;
import com.example.macbook.diplomawatch.models.PulseHistory;
import com.example.macbook.diplomawatch.models.PulseTimeChart;
import com.example.macbook.diplomawatch.services.DBService;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by macbook on 31.05.16.
 */
public class ProgramYearChartFragment extends Fragment {
    private ArrayList<PulseHistory> pulseHistories = new ArrayList<>();
    LinkedHashMap<String, ArrayList<PulseTimeChart>> pulseMap = new LinkedHashMap<>();;

    private DBService dbService;
    private long programId;

    View mView;
    Context mContext;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private LessonChartRVAdapter adapter;

    public ProgramYearChartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        programId = getArguments().getLong("programId");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_lesson_chart, container, false);
        mContext = mView.getContext();

        dbService = new DBService(mContext);
        pulseHistories = dbService.getPulseHistoryByLessonId(programId);
        setPulseHistoryToPulseMap();


        mRecyclerView = (RecyclerView) mView.findViewById(R.id.rv_lesson_chart);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new LessonChartRVAdapter(pulseMap, mContext);
        mRecyclerView.setAdapter(adapter);

        // Inflate the layout for this fragment
        return mView;
    }

    private void setPulseHistoryToPulseMap() {
        int count = pulseHistories.size();
        String currentYear = "";
        String currentMonth = "";
        int pulseValues = 0;
        int pulseCount = 0;
        ArrayList<PulseTimeChart> list = new ArrayList<>();
        for(int i = 0; i < count; i++) {
            String[] dateTime = pulseHistories.get(i).getDateTime().split("/");
            String year = dateTime[0].split("\\.")[2];
            String month = dateTime[0].split("\\.")[1];
            if(currentYear.equals("")) {
                currentYear = year;
                currentMonth = month;
                list = new ArrayList<>();
                pulseValues = pulseHistories.get(i).getPulse();
                pulseCount = 1;
            }
            else if(currentYear.equals(year)) {
                if(currentMonth.equals(month)) {
                    pulseValues += pulseHistories.get(i).getPulse();
                    pulseCount++;
                }
                else {
                    list.add(new PulseTimeChart(CommonHelper.getMonth(currentMonth), pulseValues/pulseCount, false));

                    currentMonth = month;
                    pulseValues = pulseHistories.get(i).getPulse();
                    pulseCount = 1;
                }
            }
            else {
                pulseMap.put(currentYear, list);

                currentYear = year;
                currentMonth = month;
                list = new ArrayList<>();
                pulseValues = pulseHistories.get(i).getPulse();
                pulseCount = 1;
            }
        }

        list.add(new PulseTimeChart(CommonHelper.getMonth(currentMonth), pulseValues/pulseCount, false));
        pulseMap.put(currentYear, list);

        pulseMap = CommonHelper.sortByDescending(pulseMap);
    }
}
