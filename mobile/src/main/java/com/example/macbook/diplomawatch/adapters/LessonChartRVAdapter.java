package com.example.macbook.diplomawatch.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.models.PulseTimeChart;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by macbook on 24.05.16.
 */
public class LessonChartRVAdapter extends RecyclerView.Adapter<LessonChartRVAdapter.BarChartViewHolder>{
    private HashMap<String, ArrayList<PulseTimeChart>> pulseHistoryMap;
    private Context mContext;

    public class BarChartViewHolder extends RecyclerView.ViewHolder {
        BarChart mChart;
        TextView mTextViewDate;

        public BarChartViewHolder(View itemView) {
            super(itemView);

            mChart = (BarChart) itemView.findViewById(R.id.chart1);
            mTextViewDate = (TextView) itemView.findViewById(R.id.tv_date_chart);
        }
    }
    private BarChartViewHolder barChartViewHolder;

    public LessonChartRVAdapter(HashMap<String, ArrayList<PulseTimeChart>> pulseMap, Context context) {
        pulseHistoryMap = pulseMap;
        this.mContext = context;
    }

    @Override
    public BarChartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context context = parent.getContext();

        View view = LayoutInflater.from(context)
                .inflate(R.layout.activity_barchart, parent, false);

        barChartViewHolder = new BarChartViewHolder(view);
        return barChartViewHolder;
    }

    @Override
    public void onBindViewHolder(BarChartViewHolder holder, int position) {
        drawChart(holder, position);
    }

    @Override
    public int getItemCount() {
        return pulseHistoryMap.size();
    }

    private void drawChart(BarChartViewHolder holder, int position) {
        holder.mChart.setDrawBarShadow(false);
        holder.mChart.setDrawValueAboveBar(true);

        holder.mChart.setDescription("");

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        holder.mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        holder.mChart.setPinchZoom(false);

        holder.mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);


        XAxis xAxis = holder.mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(2);

        YAxis leftAxis = holder.mChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        holder.mChart.getAxisRight().setEnabled(false);

        Legend l = holder.mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        setData(position, holder.mChart, holder.mTextViewDate);
    }

    private void setData(int position, BarChart mChart, TextView mTextViewDate) {
        ArrayList<String> xVals = new ArrayList<String>();
        String date = (String) pulseHistoryMap.keySet().toArray()[position];
        ArrayList<PulseTimeChart> pulseHistory = pulseHistoryMap.get(date);

        //set Date to TextView
        mTextViewDate.setText(date);

        int count = pulseHistory.size();
        for (int i = 0; i < count; i++) {
            xVals.add(pulseHistory.get(i).getTime() + "");
        }

        ArrayList<ArrayList<BarEntry>> allYVals = new ArrayList<ArrayList<BarEntry>>();
        ArrayList<BarEntry> yVals = new ArrayList<>();
        for(int i = 0; i < count; i++) {
            yVals.add(new BarEntry(pulseHistory.get(i).getPulseValue(), i));
        }
        if(!yVals.isEmpty()) {
            allYVals.add(yVals);
        }

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();

        int allYCount = allYVals.size();
        for(int i = 0; i < allYCount; i++) {
            BarDataSet set = new BarDataSet(allYVals.get(i),"");
            set.setBarSpacePercent(35f);
            /*
            set.setAxisDependency(YAxis.AxisDependency.LEFT);
            set.setColor(ColorTemplate.getHoloBlue());
            set.setHighLightColor(Color.rgb(244, 117, 117));
            */

            dataSets.add(set);
        }

        // create a data object with the datasets
        BarData data = new BarData(xVals, dataSets);
        //data.setValueTextColor(Color.WHITE);
        data.setValueTextSize(10f);

        // set data
        mChart.setData(data);
    }
}
