package com.example.macbook.diplomawatch.Helpers;

import android.graphics.Color;

import java.util.HashMap;

/**
 * Created by macbook on 08.06.16.
 */
public class CommonConstants {
    public static String SET_UP_MESSAGE = "Приложение сможет определять, повысился Ваш пульс или понизился, если Вы пройдете предложенные системой упражнения";
    public static String SET_DOWN_MESSAGE = "Приложение сможет определять, повысился Ваш пульс или понизился, если Вы пройдете предложенные системой упражнения";
    public static String SET_UP_AND_DOWN_MESSAGE = "Приложение сможет определять, повысился Ваш пульс или понизился, если Вы пройдете предложенные системой упражнения";

    public static HashMap<String, Integer> COLOR_SETTER = new HashMap<String, Integer>() {
        {
            put("Вдох", Color.parseColor("#5CE5E8"));
        }

        {
            put("Задержка", Color.parseColor("#B5FF7D"));
        }

        {
            put("Выдох", Color.parseColor("#23A8E8"));
        }
    };
}
