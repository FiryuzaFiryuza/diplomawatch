package com.example.macbook.diplomawatch.models;

import java.io.Serializable;

/**
 * Created by macbook on 24.12.15.
 */
public class NameValuePair implements Serializable{
    private String name;
    private String value;

    public NameValuePair(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
