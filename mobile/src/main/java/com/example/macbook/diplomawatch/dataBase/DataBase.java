package com.example.macbook.diplomawatch.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.macbook.diplomawatch.models.Label;
import com.example.macbook.diplomawatch.models.Lesson;
import com.example.macbook.diplomawatch.models.LessonProgram;
import com.example.macbook.diplomawatch.models.Program;
import com.example.macbook.diplomawatch.models.PulseHistory;
import com.example.macbook.diplomawatch.models.SelectedLabel;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by macbook on 01.12.15.
 */
public class DataBase {
    private DataBaseHelper dataBase;
    private SQLiteDatabase sqLiteDatabase;

    public DataBase(Context context) throws IOException {
        this.dataBase = new DataBaseHelper(context);
        this.dataBase.createDataBase();
        this.open();
    }

    public void close() {
        this.dataBase.close();
        this.sqLiteDatabase.close();
    }

    public ArrayList<Lesson> getLessons() {
        ArrayList<Lesson> lessonsList = new ArrayList<Lesson>();

        String selectQuery = "SELECT * FROM " + DBFields.TABLE_LESSON;

        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Lesson lesson = new Lesson();
                lesson.setId(cursor.getInt(cursor.getColumnIndex(DBFields.KEY_ID)));
                lesson.setName(cursor.getString(cursor.getColumnIndex(DBFields.KEY_NAME)));
                lesson.setDescription(cursor.getString(cursor.getColumnIndex(DBFields.KEY_DESCRIPTION)));
                lesson.setDuration(cursor.getDouble(cursor.getColumnIndex(DBFields.KEY_DURATION)));
                lesson.setIsSystemLesson(cursor.getInt(cursor.getColumnIndex(DBFields.KEY_IS_SYSTEM_LESSON)) == 1);
                lessonsList.add(lesson);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lessonsList;
    }

    public Lesson getLessonById(long id) {
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_LESSON, new String[] { DBFields.KEY_ID,
                        DBFields.KEY_NAME, DBFields.KEY_DESCRIPTION, DBFields.KEY_DURATION, DBFields.KEY_IS_SYSTEM_LESSON}, DBFields.KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Lesson lesson = new Lesson(cursor.getLong(0),
                cursor.getString(1), cursor.getString(2), cursor.getDouble(3), cursor.getInt(4) == 1);

        return lesson;
    }

    public Lesson getSystemLessonForUpDownPulse() {
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_LESSON, new String[] { DBFields.KEY_ID,
                        DBFields.KEY_NAME, DBFields.KEY_DESCRIPTION, DBFields.KEY_DURATION, DBFields.KEY_IS_SYSTEM_LESSON}, DBFields.KEY_NAME + "=?",
                new String[] { DBFields.LESSON_UP_DOWN_PULSE }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Lesson lesson = new Lesson(cursor.getLong(0),
                cursor.getString(1), cursor.getString(2), cursor.getDouble(3), cursor.getInt(4) == 1);

        return lesson;
    }

    public ArrayList<Program> getProgramsFromLesson(long id) {
        ArrayList<Program>  programsList = new ArrayList<Program>();
        Program program;
        String selectQuery;
        Cursor cursor;

        /*
        String selectQuery = "SELECT "+ TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_NAME +
                ", " + TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_CATEGORIES_ID +
                " From " + TourDataBase.TABLE_SIGHTS_FROM_TOUR +
                " INNER JOIN " + TourDataBase.TABLE_READY_TOUR + " ON " +
                TourDataBase.TABLE_SIGHTS_FROM_TOUR + "." + TourDataBase.KEY_TOUR_ID + "=" +
                TourDataBase.TABLE_READY_TOUR + "." + TourDataBase.KEY_ID +
                " INNER JOIN " + TourDataBase.TABLE_SIGHTS + " ON " +
                TourDataBase.TABLE_SIGHTS_FROM_TOUR + "." + TourDataBase.KEY_SIGHTS_ID + "=" +
                TourDataBase.TABLE_SIGHTS + "." + TourDataBase.KEY_ID +
                " WHERE " + TourDataBase.KEY_TOUR_ID + "=" + id;
        */

        selectQuery = "SELECT * " +
                " FROM " + DBFields.TABLE_PROGRAM +
                " WHERE " + DBFields.KEY_LESSON_ID + "=?";
        cursor = this.sqLiteDatabase.rawQuery(selectQuery, new String[] { String.valueOf(id) });

        if (cursor.moveToFirst()) {
            do {

                program = new Program(cursor.getLong(0),
                        cursor.getString(1), cursor.getDouble(2),
                        cursor.getInt(3)  == 1, cursor.getDouble(4),
                        cursor.getDouble(5), cursor.getDouble(6),
                        cursor.getDouble(7), cursor.getLong(8));

                programsList.add(program);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return programsList;
    }

    public Program getProgramById(long id) {
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_PROGRAM, new String[] { DBFields.KEY_ID,
                        DBFields.KEY_NAME, DBFields.KEY_DURATION,
                        DBFields.KEY_BREATH, DBFields.KEY_OUT,
                        DBFields.KEY_HOLD_FIRST, DBFields.KEY_HOLD_SECOND,
                        DBFields.KEY_UNIT, DBFields.KEY_LESSON_ID }, DBFields.KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Program program = new Program(cursor.getLong(0),
                cursor.getString(1), cursor.getDouble(2),
                cursor.getDouble(3), cursor.getDouble(4),
                cursor.getDouble(5),cursor.getDouble(6),
                cursor.getInt(7)  == 1, cursor.getLong(8));

        return program;
    }

    public void addProgram(Program program) {
        int unit = program.getUnit() ? 1 : 0;

        String selectQuery = "INSERT INTO " + DBFields.TABLE_PROGRAM + " (" +
                DBFields.KEY_LESSON_ID + ", " + DBFields.KEY_NAME + "," +
                DBFields.KEY_DURATION + "," + DBFields.KEY_UNIT + "," +
                DBFields.KEY_BREATH + "," + DBFields.KEY_OUT + "," +
                DBFields.KEY_HOLD_FIRST + "," + DBFields.KEY_HOLD_SECOND + ") values (" +
                program.getLessonId() + ", " + "\"" + program.getName() + "\"" + "," +
                program.getDuration() + "," + unit + "," +
                program.getBreath() + "," + program.getOut() + "," +
                program.getHoldFirst() + "," + program.getHoldSecond() + ");";

        this.sqLiteDatabase.execSQL(selectQuery);
    }

    public void updateProgram(Program program) {
        ContentValues values = new ContentValues();
        int unit = program.getUnit() ? 1 : 0;

        values.put(DBFields.KEY_NAME, program.getName());
        values.put(DBFields.KEY_DURATION, program.getDuration());
        values.put(DBFields.KEY_UNIT, unit);
        values.put(DBFields.KEY_BREATH, program.getBreath());
        values.put(DBFields.KEY_OUT, program.getOut());
        values.put(DBFields.KEY_HOLD_FIRST, program.getHoldFirst());
        values.put(DBFields.KEY_HOLD_SECOND, program.getHoldSecond());

        sqLiteDatabase.update(DBFields.TABLE_PROGRAM, values, "_id " + "=" + program.getId(), null);
    }

    public long addLesson(Lesson lesson) {
        ContentValues values = new ContentValues();

        values.put(DBFields.KEY_NAME, lesson.getName());
        values.put(DBFields.KEY_DESCRIPTION, lesson.getDescription());
        values.put(DBFields.KEY_IS_SYSTEM_LESSON, 0);

        sqLiteDatabase.insert(DBFields.TABLE_LESSON, null, values);

        return getLessonIdByMetadata(lesson.getName(), lesson.getDescription());
    }

    public void updateLesson(Lesson lesson) {
        ContentValues values = new ContentValues();

        values.put(DBFields.KEY_NAME, lesson.getName());
        values.put(DBFields.KEY_DESCRIPTION, lesson.getDescription());

        sqLiteDatabase.update(DBFields.TABLE_LESSON, values, "_id =" + lesson.getId(), null);
    }

    public void deleteLessonById(long id) {
        String query = "_id = " + id;
        sqLiteDatabase.delete(DBFields.TABLE_LESSON, query, null);
    }

    public void deleteProgramById(long id) {
        String query = "_id = " + id;
        sqLiteDatabase.delete(DBFields.TABLE_PROGRAM, query, null);
    }

    public void addPulse(PulseHistory pulseHistory) {
        ContentValues values = new ContentValues();

        values.put(DBFields.KEY_LESSON_ID, pulseHistory.getLessonId());
        values.put(DBFields.KEY_PROGRAM_ID, pulseHistory.getProgramId());
        values.put(DBFields.KEY_DATETIME, pulseHistory.getDateTime());
        values.put(DBFields.KEY_PULSE, pulseHistory.getPulse());
        values.put(DBFields.KEY_IS_START, pulseHistory.isStart() ? 1 : 0);

        sqLiteDatabase.insert(DBFields.TABLE_PULSE_HISTORY, null, values);
    }

    public ArrayList<PulseHistory> getPulseHistoryByLessonId(long lessonId) {
        ArrayList<PulseHistory> pulseHistories = new ArrayList<>();
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_PULSE_HISTORY, new String[]{DBFields.KEY_ID,
                        DBFields.KEY_LESSON_ID, DBFields.KEY_PROGRAM_ID,
                        DBFields.KEY_DATETIME, DBFields.KEY_PULSE,
                        DBFields.KEY_IS_START}, DBFields.KEY_LESSON_ID + "=?",
                new String[]{String.valueOf(lessonId)}, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {

                PulseHistory pulseHistory = new PulseHistory();
                pulseHistory.setId(cursor.getLong(0));
                pulseHistory.setLessonId(cursor.getLong(1));
                pulseHistory.setProgramId(cursor.getLong(2));
                pulseHistory.setDateTime(cursor.getString(3));
                pulseHistory.setPulse(cursor.getInt(4));
                pulseHistory.setIsStart(cursor.getInt(5) == 1 ? true : false);

                pulseHistories.add(pulseHistory);

            } while (cursor.moveToNext());
        }
        cursor.close();

        return pulseHistories;
    }

    public ArrayList<PulseHistory> getPulseHistoryByProgramId(long programId) {
        ArrayList<PulseHistory> pulseHistories = new ArrayList<>();
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_PULSE_HISTORY, new String[]{DBFields.KEY_ID,
                        DBFields.KEY_LESSON_ID, DBFields.KEY_PROGRAM_ID,
                        DBFields.KEY_DATETIME, DBFields.KEY_PULSE,
                        DBFields.KEY_IS_START}, DBFields.KEY_PROGRAM_ID + "=?",
                new String[]{String.valueOf(programId)}, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {

                PulseHistory pulseHistory = new PulseHistory();
                pulseHistory.setId(cursor.getLong(0));
                pulseHistory.setLessonId(cursor.getLong(1));
                pulseHistory.setProgramId(cursor.getLong(2));
                pulseHistory.setDateTime(cursor.getString(3));
                pulseHistory.setPulse(cursor.getInt(4));
                pulseHistory.setIsStart(cursor.getInt(5) == 1 ? true : false);

                pulseHistories.add(pulseHistory);

            } while (cursor.moveToNext());
        }
        cursor.close();

        return pulseHistories;
    }

    public ArrayList<PulseHistory> getPulseHistoryByLessonIdAndByProgramId(long lessonId, long programId) {
        ArrayList<PulseHistory> pulseHistories = new ArrayList<>();
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_PULSE_HISTORY, new String[]{DBFields.KEY_ID,
                        DBFields.KEY_LESSON_ID, DBFields.KEY_PROGRAM_ID,
                        DBFields.KEY_DATETIME, DBFields.KEY_PULSE,
                        DBFields.KEY_IS_START}, DBFields.KEY_LESSON_ID + "=?" + " AND " + DBFields.KEY_PROGRAM_ID + "=?",
                new String[]{String.valueOf(lessonId), String.valueOf(programId)}, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {

                PulseHistory pulseHistory = new PulseHistory();
                pulseHistory.setId(cursor.getLong(0));
                pulseHistory.setLessonId(cursor.getLong(1));
                pulseHistory.setProgramId(cursor.getLong(2));
                pulseHistory.setDateTime(cursor.getString(3));
                pulseHistory.setPulse(cursor.getInt(4));
                pulseHistory.setIsStart(cursor.getInt(5) == 1 ? true : false);

                pulseHistories.add(pulseHistory);

            } while (cursor.moveToNext());
        }
        cursor.close();

        return pulseHistories;
    }

    public ArrayList<PulseHistory> getPulseHistoryByLessonProgramDate(long lessonId, long programId, String startDate, String endDate) {
        ArrayList<PulseHistory> pulseHistories = getPulseHistoryByLessonIdAndByProgramId(lessonId, programId);
        ArrayList<PulseHistory> result = new ArrayList<>();
        if(startDate == null || endDate == null) {
            return pulseHistories;
        }
        int count = pulseHistories.size();
        for(int i = 0; i < count; i++) {
            String date = pulseHistories.get(i).getDateTime();
            if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0) {
                result.add(pulseHistories.get(i));
            }
        }

        return result;
    }

    public boolean isPulseSettedForUpDownPulseLabels(long lessonId, long programId) {
        ArrayList<PulseHistory> pulseHistories = new ArrayList<>();
        Cursor cursor = this.sqLiteDatabase.query(DBFields.TABLE_PULSE_HISTORY,
                new String[]{DBFields.KEY_ID},
                DBFields.KEY_LESSON_ID + "=?" + " AND " + DBFields.KEY_PROGRAM_ID + "=?",
                new String[]{String.valueOf(lessonId), String.valueOf(programId)}, null, null, null, null);

        boolean isSetted = false;
        if (cursor.moveToFirst()) {
            isSetted = true;
        }
        cursor.close();

        return isSetted;
    }

    private long getLessonIdByMetadata(String name, String description) {
        String selectQuery = "SELECT _id " +
                " FROM " + DBFields.TABLE_LESSON +
                " WHERE " + DBFields.KEY_NAME + "=?" + "AND " + DBFields.KEY_DESCRIPTION + "=?";
        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, new String[]{name, description});
        if (cursor != null)
            cursor.moveToFirst();

        return cursor.getLong(0);
    }

    public ArrayList<Label> getLabels() {
        ArrayList<Label> labelList = new ArrayList<>();

        String selectQuery = "SELECT * " +
                " FROM " + DBFields.TABLE_LABEL;
        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Label label = new Label();
                label.setId(cursor.getInt(cursor.getColumnIndex(DBFields.KEY_ID)));
                label.setName(cursor.getString(cursor.getColumnIndex(DBFields.KEY_NAME)));
                labelList.add(label);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return labelList;
    }

    public void addLabel(Label label) {
        ContentValues values = new ContentValues();

        values.put(DBFields.KEY_NAME, label.getName());

        sqLiteDatabase.insert(DBFields.TABLE_LABEL, null, values);
    }

    public void deleteLabelFrom(long lessonId, long programId) {
        String selectQuery = "DELETE FROM " + DBFields.TABLE_SELECTED_LABEL +
                " WHERE " + DBFields.KEY_LESSON_ID + "=" + lessonId +
                " AND " + DBFields.KEY_PROGRAM_ID + "=" + programId;
        sqLiteDatabase.execSQL(selectQuery);
    }

    public ArrayList<SelectedLabel> getSelectedLabels(long lessonId, long programId) {
        ArrayList<SelectedLabel> labelList = new ArrayList<>();

        String selectQuery = "SELECT * " +
                " FROM " + DBFields.TABLE_SELECTED_LABEL + " WHERE " +
                DBFields.KEY_LESSON_ID + "=" + lessonId + " AND " +
                DBFields.KEY_PROGRAM_ID + "=" + programId;
        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                SelectedLabel label = new SelectedLabel();
                label.setId(cursor.getInt(cursor.getColumnIndex(DBFields.KEY_ID)));
                label.setLessonId(cursor.getLong(cursor.getColumnIndex(DBFields.KEY_LESSON_ID)));
                label.setProgramId(cursor.getLong(cursor.getColumnIndex(DBFields.KEY_PROGRAM_ID)));
                label.setLabelName(cursor.getString(cursor.getColumnIndex(DBFields.KEY_LABEL_NAME)));
                labelList.add(label);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return labelList;
    }

    public void addSelectedLabels(ArrayList<SelectedLabel> selectedLabels) {
        int size = selectedLabels.size();

        for(int i = 0; i < size; i++) {
            ContentValues values = new ContentValues();

            values.put(DBFields.KEY_LESSON_ID, selectedLabels.get(i).getLessonId());
            values.put(DBFields.KEY_PROGRAM_ID, selectedLabels.get(i).getProgramId());
            values.put(DBFields.KEY_LABEL_NAME, selectedLabels.get(i).getLabelName());

            sqLiteDatabase.insert(DBFields.TABLE_SELECTED_LABEL, null, values);
        }
    }

    public ArrayList<LessonProgram> searchExercisesBySelectedLabels(ArrayList<String> selectedLabels) {
        ArrayList<SelectedLabel> exercises = searchBySelectedLabels(selectedLabels);
        ArrayList<LessonProgram> result = new ArrayList<>();

        int count = exercises.size();
        for (int i = 0; i < count; i++) {
            Lesson lesson = getLessonById(exercises.get(i).getLessonId());
            Program program = null;
            if(exercises.get(i).getProgramId() != 0) {
                program = getProgramById(exercises.get(i).getProgramId());
            }

            LessonProgram lp = new LessonProgram();
            lp.setLesson(lesson);
            lp.setProgram(program);

            result.add(lp);
        }

        return result;
    }

    public ArrayList<SelectedLabel> getByDefeniteLabel(ArrayList<String> selectedLabels) {
        ArrayList<SelectedLabel> exercises = searchBySelectedLabels(selectedLabels);

        return exercises;
    }

    public void deleteByLabel(long lessonId, long programId, String label) {
        ArrayList<SelectedLabel> slabel = getSelectedLabelsByLabel(lessonId, programId, label);
        for(int i = 0; i < slabel.size(); i++) {
            String query = "_id = " + slabel.get(i).getId();
            sqLiteDatabase.delete(DBFields.TABLE_LESSON, query, null);
        }
    }

    private ArrayList<SelectedLabel> getSelectedLabelsByLabel(long lessonId, long programId, String searchLabel) {
        ArrayList<SelectedLabel> labelList = new ArrayList<>();

        String selectQuery = "SELECT * " +
                " FROM " + DBFields.TABLE_SELECTED_LABEL + " WHERE " +
                DBFields.KEY_LESSON_ID + "=" + lessonId + " AND " +
                DBFields.KEY_PROGRAM_ID + "=" + programId + " AND " +
                DBFields.KEY_LABEL_NAME + "='" + searchLabel + "'";
        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                SelectedLabel label = new SelectedLabel();
                label.setId(cursor.getInt(cursor.getColumnIndex(DBFields.KEY_ID)));
                label.setLessonId(cursor.getLong(cursor.getColumnIndex(DBFields.KEY_LESSON_ID)));
                label.setProgramId(cursor.getLong(cursor.getColumnIndex(DBFields.KEY_PROGRAM_ID)));
                label.setLabelName(cursor.getString(cursor.getColumnIndex(DBFields.KEY_LABEL_NAME)));
                labelList.add(label);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return labelList;
    }


    private ArrayList<SelectedLabel> searchBySelectedLabels(ArrayList<String> selectedLabels) {
        ArrayList<SelectedLabel> labelList = new ArrayList<>();
        String query = "";
        for(int i = 0; i < selectedLabels.size(); i++) {
            if (i > 0) {
                query += " OR ";
            }
            query += DBFields.KEY_LABEL_NAME + "= '" + selectedLabels.get(i) + "'";
        }
        String selectQuery = "SELECT * " +
                " FROM " + DBFields.TABLE_SELECTED_LABEL + " WHERE " + query + ";";
        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                SelectedLabel label = new SelectedLabel();
                label.setId(cursor.getInt(cursor.getColumnIndex(DBFields.KEY_ID)));
                label.setLessonId(cursor.getLong(cursor.getColumnIndex(DBFields.KEY_LESSON_ID)));
                label.setProgramId(cursor.getLong(cursor.getColumnIndex(DBFields.KEY_PROGRAM_ID)));
                label.setLabelName(cursor.getString(cursor.getColumnIndex(DBFields.KEY_LABEL_NAME)));
                labelList.add(label);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return filter(labelList);
    }

    private ArrayList<SelectedLabel> filter(ArrayList<SelectedLabel> labelList) {
        ArrayList<SelectedLabel> result = new ArrayList<SelectedLabel>();
        int count = labelList.size();
        for(int i = 0; i < count; i++) {
            boolean isEqual = false;
            for(int j = 0; j < result.size() && !isEqual; j++) {
                if(result.get(j).getProgramId() == labelList.get(i).getProgramId() && result.get(j).getLessonId() == labelList.get(i).getLessonId()) {
                    isEqual = true;
                }
            }
            if(!isEqual) {
                result.add(labelList.get(i));
            }
        }

        return result;
    }

    public boolean isUpPulseLabelSetted() {
        String selectQuery = "SELECT " + DBFields.KEY_IS_SETTED +
                " FROM " + DBFields.TABLE_CHECK_DEFAULT_LABELS + " WHERE " +
                DBFields.KEY_LABEL_NAME + "='" + DBFields.UP_PULSE_LABEL + "';";
        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null)
            cursor.moveToFirst();

        return cursor.getInt(0) == 1;
    }

    public boolean isDownPulseLabelSetted() {
        String selectQuery = "SELECT " + DBFields.KEY_IS_SETTED +
                " FROM " + DBFields.TABLE_CHECK_DEFAULT_LABELS + " WHERE " +
                DBFields.KEY_LABEL_NAME + "='" + DBFields.DOWN_PULSE_LABEL + "';";
        Cursor cursor = this.sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null)
            cursor.moveToFirst();

        return cursor.getInt(0) == 1;
    }

    public void upPulseSetted() {
        ContentValues values = new ContentValues();

        values.put(DBFields.KEY_IS_SETTED, 1);

        sqLiteDatabase.update(DBFields.TABLE_CHECK_DEFAULT_LABELS, values, DBFields.KEY_LABEL_NAME + " ='" + DBFields.UP_PULSE_LABEL + "'", null);
    }

    public void downPulseSetted() {
        ContentValues values = new ContentValues();

        values.put(DBFields.KEY_IS_SETTED, 1);

        sqLiteDatabase.update(DBFields.TABLE_CHECK_DEFAULT_LABELS, values, DBFields.KEY_LABEL_NAME + " ='" + DBFields.DOWN_PULSE_LABEL + "'", null);
    }

    private void open() throws SQLException {
        sqLiteDatabase = dataBase.getWritableDatabase();
    }
}
