package com.example.macbook.diplomawatch;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import com.example.macbook.diplomawatch.adapters.ButtonAdapter;
import com.example.macbook.diplomawatch.adapters.SearchExerciseRVAdapter;
import com.example.macbook.diplomawatch.models.Label;
import com.example.macbook.diplomawatch.models.LessonProgram;
import com.example.macbook.diplomawatch.services.DBService;

import java.util.ArrayList;

/**
 * Created by macbook on 27.05.16.
 */
public class SearchExerciseActivity extends BaseActivity implements View.OnClickListener {
    public static ArrayList<String> userSelections = new ArrayList<>();

    private DBService dbService = new DBService(this);
    ArrayList<Label> labelList;
    ArrayList<LessonProgram> result = new ArrayList<>();

    private SearchExerciseRVAdapter exerciseRVAdapter = null;

    private GridView mGridview;
    private Button mButtonSearch;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_exe);

        initToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        labelList = dbService.getLabels();

        mGridview = (GridView) findViewById(R.id.gv_selection_exe);
        mGridview.setAdapter(new ButtonAdapter(this, labelList, null));

        mButtonSearch = (Button) findViewById(R.id.bttn_search);
        mButtonSearch.setOnClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_found_exec);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.removeItem(R.id.action_save);
        menu.removeItem(R.id.action_statistics);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(userSelections.size() != 0) {
            result = dbService.searchBySelectedLabels(userSelections);
            if(exerciseRVAdapter == null && result.size() > 0) {
                exerciseRVAdapter = new SearchExerciseRVAdapter(result, this);
                mRecyclerView.setAdapter(exerciseRVAdapter);
            }
            else if(result.size() > 0) {
                exerciseRVAdapter.notify(result);
            }
            //mRecyclerView.getAdapter().notifyDataSetChanged();
            //exerciseRVAdapter.notifyDataSetChanged();
        }
    }
}
