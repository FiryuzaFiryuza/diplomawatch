package com.example.macbook.diplomawatch.services;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by macbook on 29.02.16.
 */
public class SendToDataLayerThread extends Thread {
    String path;
    String[] message;
    GoogleApiClient mGoogleApiClient;
    String sendMsg = new String();

    // Constructor to send a message to the data layer
    public SendToDataLayerThread(GoogleApiClient googleApiClient, String p, String[] msg) {
        path = p;
        message = msg;
        sendMsg = message[0] + "-" + message[1] + "-" + message[2] + "-" + message[3] + "-" + message[4];
        mGoogleApiClient = googleApiClient;
    }

    public void run() {
        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : nodes.getNodes()) {
            MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleApiClient, node.getId(), path, sendMsg.getBytes()).await();
            if (result.getStatus().isSuccess()) {
                Log.d("TAG", "Message: {" + sendMsg + "} sent to: " + node.getDisplayName());
            } else {
                // Log an error
                Log.d("TAG", "ERROR: failed to send Message");
            }
        }
    }
}
