package com.example.macbook.diplomawatch.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.macbook.diplomawatch.Helpers.CommonConstants;
import com.example.macbook.diplomawatch.LessonDetails;
import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.animation.ColorizeActivity;
import com.example.macbook.diplomawatch.dataBase.DBFields;
import com.example.macbook.diplomawatch.models.Lesson;
import com.example.macbook.diplomawatch.models.NameValuePair;
import com.example.macbook.diplomawatch.services.DBService;
import com.example.macbook.diplomawatch.services.DialogService;
import com.example.macbook.diplomawatch.services.NumberPickerService;
import com.marshalchen.ultimaterecyclerview.SwipeableUltimateViewAdapter;
import com.marshalchen.ultimaterecyclerview.URLogs;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.swipe.SwipeLayout;

import java.util.ArrayList;

/**
 * Created by macbook on 28.03.16.
 */
public class LessonSwipeAdapter extends SwipeableUltimateViewAdapter implements NumberPicker.OnValueChangeListener{

    private ArrayList<Lesson> mLessons;
    private double userPulse = 80;
    private Context context;

    private DBService dbService;

    public LessonSwipeAdapter(ArrayList<Lesson> list, Context context) {
        mLessons = list;
        this.context = context;
        dbService = new DBService(context);
    }

    @Override
    public UltimateRecyclerviewViewHolder getViewHolder(View view) {
        return new UltimateRecyclerviewViewHolder(view);
    }

    @Override
    public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup parent) {
        final Context context = parent.getContext();

        View view = LayoutInflater.from(context)
                .inflate(R.layout.lesson_cards_layout, parent, false);

        final LessonViewHolder lessonViewHolder = new LessonViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                URLogs.d("click");
            }
        });
        SwipeLayout swipeLayout = lessonViewHolder.swipeLayout;
        swipeLayout.setDragEdge(SwipeLayout.DragEdge.Right);
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        /*swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
                Toast.makeText(context, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });*/

        /*swipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, LessonDetails.class);
                intent.putExtra("lesson", mLessons.get(lessonViewHolder.getAdapterPosition()).getId());
                Log.d("Click", "Click");
                context.startActivity(intent);
            }
        });*/

        swipeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, LessonDetails.class);
                intent.putExtra("lesson", mLessons.get(lessonViewHolder.getAdapterPosition()).getId());
                Log.d("Click", "Click");
                context.startActivity(intent);
                return true;
            }
        });

        lessonViewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLessons.get(lessonViewHolder.getAdapterPosition()).getDuration() > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Ошибка!")
                            .setMessage("Занятие содержит в себе упражнения!")
                            .setCancelable(false)
                            .setNegativeButton("ОК",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else {
                    remove(lessonViewHolder.getPosition());
                    Toast.makeText(v.getContext(), "Deleted ", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return lessonViewHolder;
    }

    @Override
    public void onBindViewHolder(final UltimateRecyclerviewViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        ((LessonViewHolder)holder).name.setText(mLessons.get(position).getName());
        ((LessonViewHolder)holder).description.setText(mLessons.get(position).getDescription());
        ((LessonViewHolder)holder).bttnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(settedDefaultLabels(position)) {
                    show(position);
                }
            }
        });
        ((LessonViewHolder)holder).view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, LessonDetails.class);
                intent.putExtra("lesson", mLessons.get(position).getId());
                Log.d("Click", "Click");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mLessons.size();
    }

    @Override
    public int getAdapterItemCount() {
        return mLessons.size();
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(Lesson lesson) {
        insert(lesson, mLessons.size());
    }

    public void insert(Lesson lesson, int index) {
        closeAllExcept(null);

        mLessons.add(index, lesson);
        notifyItemInserted(index);
    }

    public void remove(int position) {
        long id = mLessons.get(position).getId();
        dbService.deleteLesson(id);

        mLessons.remove(position);
        closeItem(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    private void show(final int position)
    {
        NumberPickerService numberPickerService = new NumberPickerService(context, "Введите пульс", 0, 150);

        final Dialog d = numberPickerService.getmDialog();

        Button mBttnOk = numberPickerService.getmBttnOk();
        Button mBttnCancel = numberPickerService.getmBttnCancel();

        final NumberPicker mNumPicker = numberPickerService.getmNumberPicker();
        mNumPicker.setDisplayedValues(generateFloatNumbers());
        mNumPicker.setOnValueChangedListener(this);

        mBttnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPulse = (double) mNumPicker.getValue() + 50;

                ArrayList<NameValuePair> listForSend = new ArrayList<NameValuePair>();
                listForSend.add(new NameValuePair("lesson", String.valueOf(mLessons.get(position).getId())));
                listForSend.add(new NameValuePair("pulse", String.valueOf(userPulse)));

                Bundle extra = new Bundle();
                extra.putSerializable("objects", listForSend);

                Context context = v.getContext();
                Intent intent = new Intent(context, ColorizeActivity.class);
                intent.putExtra("start", extra);
                context.startActivity(intent);

                d.dismiss();
            }
        });
        mBttnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();
    }

    public boolean settedDefaultLabels(int position) {
        boolean isUpSetted = dbService.isUpPulseLabelSetted();
        boolean isDownSetted = dbService.isDownPulseLabelSetted();

        String message = "";
        if(!isUpSetted && !isDownSetted) {
            message = CommonConstants.SET_UP_AND_DOWN_MESSAGE;
        }
        else if(!isDownSetted) {
            message = CommonConstants.SET_DOWN_MESSAGE;
        }
        else if(!isUpSetted) {
            message = CommonConstants.SET_UP_MESSAGE;
        }

        boolean found = false;
        if(!isUpSetted || !isDownSetted) {
            int count = mLessons.size();
            long id = 0;
            for(int i = 0; i < count && !found; i++) {
                if(mLessons.get(i).getName().equals(DBFields.LESSON_UP_DOWN_PULSE)) {
                    found = true;
                    id = mLessons.get(i).getId();
                }
            }
            if(found) {
                showForSetLabels(message, id, position);
            }
        }
        return isUpSetted && isDownSetted;
    }

    public void showForSetLabels(String message, final long id, final int position) {
        DialogService dialogService = new DialogService(context, "???", message);

        final Dialog d = dialogService.getmDialog();

        Button mBttnOk = dialogService.getmBttnOk();
        Button mBttnCancel = dialogService.getmBttnCancel();

        mBttnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Context context = v.getContext();
                Intent intent = new Intent(context, LessonDetails.class);
                intent.putExtra("lesson", id);
                context.startActivity(intent);

                d.dismiss();
            }
        });
        mBttnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
                show(position);
            }
        });
        d.show();
    }


    private String[] generateFloatNumbers() {
        String[] numbers = new String[251];
        for (int i = 0; i < 251; i ++) {
            numbers[i] = String.valueOf(i + 50);
        }

        return numbers;
    }

    public class LessonViewHolder extends UltimateRecyclerviewViewHolder {
        View view;
        ImageView photo;
        TextView name;
        TextView description;
        Button bttnStart;

        public Button deleteButton;

        public LessonViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            photo = (ImageView) itemView.findViewById(R.id.iv_photo);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            description = (TextView) itemView.findViewById(R.id.tv_description);
            bttnStart = (Button) itemView.findViewById(R.id.bttn_start_lesson);

            deleteButton = (Button) itemView.findViewById(R.id.delete);
        }
    }
}