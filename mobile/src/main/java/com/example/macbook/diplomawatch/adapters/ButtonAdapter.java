package com.example.macbook.diplomawatch.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;

import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.SearchExerciseActivity;
import com.example.macbook.diplomawatch.models.Label;
import com.example.macbook.diplomawatch.models.SelectedLabel;

import java.util.ArrayList;

/**
 * Created by macbook on 27.05.16.
 */
public class ButtonAdapter extends BaseAdapter {
    private Context mContext;
    public ArrayList<Label> labelList;

    // Gets the context so it can be used later
    public ButtonAdapter(Context c, ArrayList<Label> labels, ArrayList<SelectedLabel> selectedLabels) {

        mContext = c;
        labelList = labels;
        SearchExerciseActivity.userSelections = new ArrayList<>();
        if(selectedLabels != null) {
            setToUserSelections(selectedLabels);
        }
    }

    // Total number of things contained within the adapter
    public int getCount() {
        return labelList.size();
    }

    // Require for structure, not really used in my code.
    public Object getItem(int position) {
        return null;
    }

    // Require for structure, not really used in my code. Can
    // be used to get the id of an item in the adapter for
    // manual control.
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position,
                        View convertView, ViewGroup parent) {
        Button btn;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            btn = new Button(mContext);
            btn.setLayoutParams(new GridView.LayoutParams(400, 100));
            btn.setTextSize(12);
            btn.setPadding(8, 8, 8, 8);
            //btn.setBackgroundColor(Color.parseColor("#fff5f5f5"));
            //Drawable image=(Drawable)mContext.getResources().getDrawable(R.drawable.tag_selected_button);
        }
        else {
            btn = (Button) convertView;
        }

        btn.setText(labelList.get(position).getName());
        // filenames is an array of strings

        if(SearchExerciseActivity.userSelections.contains(labelList.get(position).getName())) {
            btn.setTextColor(Color.parseColor("#FF4081"));
            btn.setId(position);
            btn.setBackgroundResource(R.drawable.tag_selected_button);
        }
        else {
            btn.setTextColor(Color.parseColor("#2ABCFF"));
            btn.setId(-1);
            btn.setBackgroundResource(R.drawable.tag_not_selected_button);
        }
        btn.setOnClickListener(new ButtonClickListener(labelList.get(position).getName(), position));

        return btn;
    }

    private void setToUserSelections(ArrayList<SelectedLabel> labels) {
        int count = labels.size();
        for(int i = 0; i < count; i++) {
            SearchExerciseActivity.userSelections.add(labels.get(i).getLabelName());
        }
    }

    private class ButtonClickListener implements View.OnClickListener {
        private int position;
        private String selection;

        public ButtonClickListener(String selection, int position)
        {
            this.selection = selection;
            this.position = position;
        }

        public void onClick(View v)
        {
            if(v.getId() == -1) {
                v.setId(position);
                ((Button)v).setTextColor(Color.parseColor("#FF4081"));
                SearchExerciseActivity.userSelections.add(selection);
                ((Button)v).setBackgroundResource(R.drawable.tag_selected_button);
            }
            else {
                v.setId(-1);
                ((Button)v).setTextColor(Color.parseColor("#2ABCFF"));
                ((Button)v).setBackgroundResource(R.drawable.tag_not_selected_button);
                removeSelection();
            }
        }

        private void removeSelection() {
            boolean found = false;
            int id = -1;
            int count = SearchExerciseActivity.userSelections.size();
            for(int i = 0; i < count && !found; i++) {
                if(SearchExerciseActivity.userSelections.get(i).equals(selection)) {
                    found = true;
                    id = i;
                }
            }
            if(found) {
                SearchExerciseActivity.userSelections.remove(id);
            }
        }
    }
}
