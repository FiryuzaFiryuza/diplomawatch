package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 12.05.16.
 */
public class PulseTimeChart {
    private String Time;
    private int PulseValue;
    private boolean IsStart;

    public PulseTimeChart(String time, int pulseValue, boolean isStart) {
        Time = time;
        PulseValue = pulseValue;
        IsStart = isStart;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public int getPulseValue() {
        return PulseValue;
    }

    public void setPulseValue(int pulseValue) {
        PulseValue = pulseValue;
    }

    public boolean isStart() {
        return IsStart;
    }

    public void setIsStart(boolean isStart) {
        IsStart = isStart;
    }
}
