package com.example.macbook.diplomawatch.services;

import android.content.Context;

import com.example.macbook.diplomawatch.dataBase.DataBase;
import com.example.macbook.diplomawatch.models.Label;
import com.example.macbook.diplomawatch.models.Lesson;
import com.example.macbook.diplomawatch.models.LessonProgram;
import com.example.macbook.diplomawatch.models.Program;
import com.example.macbook.diplomawatch.models.PulseHistory;
import com.example.macbook.diplomawatch.models.SelectedLabel;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by macbook on 10.12.15.
 */
public class DBService {
    private DataBase dataBase;
    private Context context;

    public DBService(Context context) {
        this.context = context;
    }

    public ArrayList<Lesson> getLessons() {
        openDB();
        assert dataBase != null;
        final ArrayList<Lesson> lessons = dataBase.getLessons();
        closeDB();

        return lessons;
    }

    public Lesson getLessonById(long id) {
        openDB();
        assert dataBase != null;
        Lesson lesson = dataBase.getLessonById(id);
        closeDB();

        return lesson;
    }

    public ArrayList<Program> getProgramsFromLesson(long id) {
        openDB();
        assert dataBase != null;
        ArrayList<Program> programs = dataBase.getProgramsFromLesson(id);
        closeDB();

        return programs;
    }

    public void updateLesson(Lesson lesson){
        openDB();
        dataBase.updateLesson(lesson);
        closeDB();
    }

    public long addLesson(Lesson lesson) {
        openDB();
        long id = dataBase.addLesson(lesson);
        closeDB();

        return id;
    }

    public Program getProgramById(long id) {
        openDB();
        assert dataBase != null;
        Program program = dataBase.getProgramById(id);
        closeDB();

        return program;
    }

    public void addProgram(Program program) {
        openDB();
        assert dataBase != null;
        dataBase.addProgram(program);
        closeDB();
    }

    public void updateProgram(Program program) {
        openDB();
        assert dataBase != null;
        dataBase.updateProgram(program);
        closeDB();
    }

    public void deleteLesson(long id) {
        openDB();
        assert dataBase != null;
        dataBase.deleteLessonById(id);
        closeDB();
    }

    public void deleteProgram(long id) {
        openDB();
        assert dataBase != null;
        dataBase.deleteProgramById(id);
        closeDB();
    }

    public void addPulse(PulseHistory pulseHistory) {
        openDB();
        assert dataBase != null;
        dataBase.addPulse(pulseHistory);
        closeDB();
    }

    public ArrayList<PulseHistory> getPulseHistoryByLessonId(long lessonId) {
        openDB();
        assert dataBase != null;
        ArrayList<PulseHistory> pulseHistories = dataBase.getPulseHistoryByLessonId(lessonId);
        closeDB();

        return pulseHistories;
    }

    public ArrayList<PulseHistory> getPulseHistoryByProgramId(long programId) {
        openDB();
        assert dataBase != null;
        ArrayList<PulseHistory> pulseHistories = dataBase.getPulseHistoryByProgramId(programId);
        closeDB();

        return pulseHistories;
    }

    public ArrayList<PulseHistory> getPulseHistoryByLessonIdAndByProgramId(long lessonId, long programId) {
        openDB();
        assert dataBase != null;
        ArrayList<PulseHistory> pulseHistories = dataBase.getPulseHistoryByLessonIdAndByProgramId(lessonId, programId);
        closeDB();

        return pulseHistories;
    }

    public ArrayList<Label> getLabels() {
        openDB();
        assert dataBase != null;
        final ArrayList<Label> labels = dataBase.getLabels();
        closeDB();

        return labels;
    }

    public void addLabel(Label label) {
        openDB();
        assert dataBase != null;
        dataBase.addLabel(label);
        closeDB();
    }

    public void deleteSelectedLabels(long lessonId, long programId) {
        openDB();
        assert dataBase != null;
        dataBase.deleteLabelFrom(lessonId, programId);
        closeDB();
    }

    public ArrayList<SelectedLabel> getSelectedLabels(long lessonId, long programId) {
        openDB();
        assert dataBase != null;
        final ArrayList<SelectedLabel> labels = dataBase.getSelectedLabels(lessonId, programId);
        closeDB();

        return labels;
    }

    public void addSelectedLabels(ArrayList<SelectedLabel> labels) {
        openDB();
        assert dataBase != null;
        dataBase.addSelectedLabels(labels);
        closeDB();
    }

    public ArrayList<LessonProgram> searchBySelectedLabels(ArrayList<String> labels) {
        openDB();
        assert dataBase != null;
        ArrayList<LessonProgram> result = dataBase.searchExercisesBySelectedLabels(labels);
        closeDB();

        return result;
    }

    public boolean isUpPulseLabelSetted() {
        openDB();
        assert dataBase != null;
        boolean result = dataBase.isUpPulseLabelSetted();
        closeDB();

        return result;
    }

    public boolean isDownPulseLabelSetted() {
        openDB();
        assert dataBase != null;
        boolean result = dataBase.isDownPulseLabelSetted();
        closeDB();

        return result;
    }

    public Lesson getSystemLessonForUpDownPulse() {
        openDB();
        assert dataBase != null;
        Lesson lesson = dataBase.getSystemLessonForUpDownPulse();
        closeDB();

        return lesson;
    }

    public void upPulseSetted() {
        openDB();
        assert dataBase != null;
        dataBase.upPulseSetted();
        closeDB();
    }

    public void downPulseSetted() {
        openDB();
        assert dataBase != null;
        dataBase.downPulseSetted();
        closeDB();
    }

    public boolean isPulseSettedForUpDownPulseLabels(long lessonId, long programId) {
        openDB();
        assert dataBase != null;
        boolean result = dataBase.isPulseSettedForUpDownPulseLabels(lessonId, programId);
        closeDB();

        return result;
    }

    public ArrayList<SelectedLabel> getByDefeniteLabel(ArrayList<String> label) {
        openDB();
        assert dataBase != null;
        ArrayList<SelectedLabel> result = dataBase.getByDefeniteLabel(label);
        closeDB();

        return result;
    }

    public ArrayList<PulseHistory> getPulseHistoryByLessonProgramDate(long lessonId, long programId, String startDate, String endDate) {
        openDB();
        assert dataBase != null;
        ArrayList<PulseHistory> pulseHistories = dataBase.getPulseHistoryByLessonProgramDate(lessonId, programId, startDate, endDate);
        closeDB();

        return pulseHistories;
    }

    public void deleteByLabel(long lessonId, long programId, String label) {
        openDB();
        assert dataBase != null;
        dataBase.deleteByLabel(lessonId, programId, label);
        closeDB();
    }

    private void openDB() {
        dataBase = null;
        try {
            dataBase = new DataBase(context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeDB() {
        dataBase.close();
    }
}
