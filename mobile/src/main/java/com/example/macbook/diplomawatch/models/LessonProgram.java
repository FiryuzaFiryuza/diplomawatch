package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 30.05.16.
 */
public class LessonProgram {
    private Lesson Lesson;
    private Program Program;

    public com.example.macbook.diplomawatch.models.Lesson getLesson() {
        return Lesson;
    }

    public void setLesson(com.example.macbook.diplomawatch.models.Lesson lesson) {
        Lesson = lesson;
    }

    public com.example.macbook.diplomawatch.models.Program getProgram() {
        return Program;
    }

    public void setProgram(com.example.macbook.diplomawatch.models.Program program) {
        Program = program;
    }
}
