package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 17.11.15.
 */
public class Program extends PersistentObject{

    private String Name;

    private double Duration;

    // 1 - Секунды; 0 - Кардио
    private boolean Unit;

    private double Breath;

    private double HoldFirst;

    private double Out;

    private double HoldSecond;

    private long LessonId;

    public Program(long id, String name, double duration,
                   double breath, double out,
                   double holdFirst, double holdSecond,
                   boolean unit, long lessonId)
    {
        super(id);
        Name = name;
        Duration = duration;
        Breath = breath;
        Out = out;
        HoldFirst = holdFirst;
        HoldSecond = holdSecond;
        Unit = unit;
        LessonId = lessonId;
    }

    public Program(long id, String name, double duration,
                   boolean unit, double breath,
                   double out, double holdFirst,
                   double holdSecond, long lessonId)
    {
        super(id);
        Name = name;
        Duration = duration;
        Breath = breath;
        Out = out;
        HoldFirst = holdFirst;
        HoldSecond = holdSecond;
        Unit = unit;
        LessonId = lessonId;
    }

    public Program() {

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getDuration() {
        return Duration;
    }

    public void setDuration(double duration) {
        Duration = duration;
    }

    public boolean getUnit() {
        return Unit;
    }

    public void setUnit(boolean unit) {
        Unit = unit;
    }

    public double getBreath() {
        return Breath;
    }

    public void setBreath(double breath) {
        Breath = breath;
    }

    public double getHoldFirst() {
        return HoldFirst;
    }

    public void setHoldFirst(double holdFirst) {
        HoldFirst = holdFirst;
    }

    public double getOut() {
        return Out;
    }

    public void setOut(double out) {
        Out = out;
    }

    public double getHoldSecond() {
        return HoldSecond;
    }

    public void setHoldSecond(double holdSecond) {
        HoldSecond = holdSecond;
    }

    public long getLessonId() {
        return LessonId;
    }

    public void setLessonId(long lessonId) {
        LessonId = lessonId;
    }
}
