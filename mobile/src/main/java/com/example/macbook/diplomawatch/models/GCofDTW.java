package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 10.06.16.
 */
public class GCofDTW {
    private double GC;
    private String Label;


    public GCofDTW(double gc, String label) {
        GC = gc;
        Label = label;
    }

    public double getGC() {
        return GC;
    }

    public void setGC(double GC) {
        this.GC = GC;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }
}
