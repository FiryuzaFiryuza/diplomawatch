package com.example.macbook.diplomawatch.animation;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.macbook.diplomawatch.Helpers.CommonConstants;
import com.example.macbook.diplomawatch.MainActivity;
import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.models.NameValuePair;
import com.example.macbook.diplomawatch.models.Program;
import com.example.macbook.diplomawatch.services.DBService;
import com.example.macbook.diplomawatch.services.DataLayerListenerService;
import com.example.macbook.diplomawatch.services.DefineLabelService;
import com.example.macbook.diplomawatch.services.SendToDataLayerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by macbook on 23.11.15.
 */
public class ColorizeActivity extends AppCompatActivity implements
        SurfaceHolder.Callback,
        NumberPicker.OnValueChangeListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private long lessonId;
    private ArrayList<Program> programList;

    private boolean isStart = true;

    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;

    private Paint mPaint;
    private RectF mOval;
    private Paint mArcPaint;
    private float mAngle;
    private float mPartAngle;
    private Timer timer;
    private double programDuration;
    private double curDuration;
    private int count;
    private int startTimer;
    private String startPosition;
    private String curPosition = "Вдох";
    private int curItem = 0;

    private int sizeOfPrograms;
    private double lessonDuration;

    private FloatingActionButton mBttnControl;
    private ImageView mBttnClose;
    private ImageView mBttnNext;
    private ImageView mBttnPrev;
    private ImageView heartImage;
    Animation mHeartBeatAnimation;

    private boolean isStopped = false;
    private boolean hasCardio = false;
    private float userPulse = 80;
    private boolean isSeconds = false;
    private float curPulse, prevPulse = -1;

    private int heightScreen;
    private int widthScreen;

    private String path = "/exercise/animation";
    private String[] positionToWatch = new String[5];

    private DBService dbService = new DBService(this);
    GoogleApiClient mGoogleApiClient;
    AnimationService animationService = new AnimationService(this);

    TextView textView;
    private Handler heartBeatHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // message from API client! message from wear! The contents is the heartbeat.
            if (textView != null) {
                curPulse = msg.what;
                if(curPulse > 0 && prevPulse != curPulse) {
                    prevPulse = curPulse;
                    isStart = animationService.addNewPulseValue(isStopped, isStart, prevPulse, lessonId, sizeOfPrograms - count, programList);
                }

                String value;
                if(curPulse > 0) {
                    value = Integer.toString((int)curPulse);
                }
                else {
                    value = Integer.toString((int)userPulse);
                }
                textView.setText(value);
            }
        }
    };

    private Handler animationStateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // message from API client! message from wear! The contents is the heartbeat.
            String state = (String) msg.obj;
            if(state.equalsIgnoreCase("stop") || state.equalsIgnoreCase("play")) {
                isStopped = state.equalsIgnoreCase("stop") ? true : false;
                controlAnimation();
            }
            else {
                switch(state) {
                    case "next" :
                        setNextProgram();
                        break;
                    case "previous" :
                        setPreviousProgram();
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        heightScreen = metrics.heightPixels;
        widthScreen = metrics.widthPixels;

        initGoogleApiClient();
        initializeControls();

        Bundle extra = getIntent().getBundleExtra("start");
        ArrayList<NameValuePair> objects = (ArrayList<NameValuePair>) extra.getSerializable("objects");

        //Bundle id = getIntent().getExtras();
        lessonId = Long.parseLong(objects.get(0).getValue());
        userPulse = Float.parseFloat(objects.get(1).getValue());
        curPulse = userPulse;

        textView = (TextView) findViewById(R.id.heartbeat);
        textView.setText(String.valueOf(Float.toString(curPulse)));

        programList = dbService.getProgramsFromLesson(lessonId);
        sizeOfPrograms = programList.size();

        if (sizeOfPrograms == 0) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("noLessons", -1);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        }
        lessonDuration = 0;
        for (Program pr : programList) {
            if (!pr.getUnit()) {
                hasCardio = true;
            }

            lessonDuration += pr.getDuration();
        }
        lessonDuration *= 1000;

        createSurfaceView();
    }

    private void createSurfaceView() {
        mSurfaceView = (SurfaceView) findViewById(R.id.sv_colorize);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
    }

    private void initializeControls() {
        mBttnControl = (FloatingActionButton) findViewById(R.id.bttn_control);
        mBttnControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isStopped = !isStopped;

                mBttnControl.setVisibility(View.INVISIBLE);
                mBttnClose.setVisibility(View.INVISIBLE);
                mBttnNext.setVisibility(View.INVISIBLE);
                mBttnPrev.setVisibility(View.INVISIBLE);
            }
        });

        mBttnClose = (ImageView) findViewById(R.id.bttn_close_animation);
        mBttnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                Intent intent = new Intent(ColorizeActivity.this, MainActivity.class);
                intent.putExtra("return", -1);
                startActivity(intent);
            }
        });

        mBttnNext = (ImageView) findViewById(R.id.bttn_next);
        mBttnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNextProgram();
            }
        });

        mBttnPrev = (ImageView) findViewById(R.id.bttn_back);
        mBttnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPreviousProgram();
            }
        });

        View mainView = findViewById(R.id.rl_animation);
        mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStopped = !isStopped;
                isStart = !isStopped;
                controlAnimation();
            }
        });

        heartImage = (ImageView) findViewById(R.id.iv_heart);
        mHeartBeatAnimation = AnimationUtils.loadAnimation(ColorizeActivity.this, R.anim.heart_pulse);
        heartImage.startAnimation(mHeartBeatAnimation);
    }

    private void controlAnimation() {
        if (isStopped) {
            mBttnControl.setVisibility(View.VISIBLE);
            mBttnClose.setVisibility(View.VISIBLE);

            if (count > 1)
                mBttnNext.setVisibility(View.VISIBLE);

            if (sizeOfPrograms - count > 0)
                mBttnPrev.setVisibility(View.VISIBLE);
        } else {
            mBttnControl.setVisibility(View.INVISIBLE);
            mBttnClose.setVisibility(View.INVISIBLE);
            mBttnNext.setVisibility(View.INVISIBLE);
            mBttnPrev.setVisibility(View.INVISIBLE);
        }
    }

    private void setNextProgram() {
        count--;
        int id = sizeOfPrograms - count;
        setNextOrPreviousProgram(id);
    }

    private void setPreviousProgram() {
        count++;
        int id = sizeOfPrograms - count;
        setNextOrPreviousProgram(id);
    }

    private void setNextOrPreviousProgram(int id) {
        isStart = true;
        curPosition = "Вдох";
        curDuration = programList.get(id).getBreath() * 1000;
        programDuration = programList.get(id).getDuration() * 1000;
        startTimer = 3000;
        startPosition = "Начинаем";

        mAngle = 0;
        mPartAngle = 36000 / (float) curDuration;

        setLessonDuration(id);

        mBttnControl.setVisibility(View.INVISIBLE);
        mBttnClose.setVisibility(View.INVISIBLE);
        mBttnNext.setVisibility(View.INVISIBLE);
        mBttnPrev.setVisibility(View.INVISIBLE);

        isStopped = false;
    }

    private void setLessonDuration(int id) {
        lessonDuration = 0;
        for (int i = id; i < sizeOfPrograms; i++) {
            lessonDuration += programList.get(i).getDuration();
        }
        lessonDuration *= 1000;
    }

    private void initGoogleApiClient() {
        if (null == mGoogleApiClient) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            Log.v("TAG", "GoogleApiClient created");
        }

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            Log.v("TAG", "Connecting to GoogleApiClient..");
        }

        startService(new Intent(this, DataLayerListenerService.class));
    }



    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        isSeconds = programList.get(0).getUnit();
        float p = userPulse;
        if (curPulse != 0) {
            p = curPulse;
        }
        float oneBit = 1000;

        if (!isSeconds) {
            oneBit = (60000 / p);
        }
        lessonDuration = 1000 * lessonDuration;
        programDuration = programList.get(0).getDuration() * oneBit;
        curDuration = programList.get(0).getBreath() * oneBit;

        count = programList.size();
        mPartAngle = 36000 / (float) curDuration; // durationOfAllPrograms
        mAngle = 0;

        float heartCycle = (float) (lessonDuration / oneBit);
        mHeartBeatAnimation.setInterpolator(new CycleInterpolator(heartCycle));
        mHeartBeatAnimation.startNow();
        mHeartBeatAnimation.setDuration((long) lessonDuration);

        startTimer = 3000;
        startPosition = "Начинаем";
        timer = new Timer("ColorizeScreen", true);
        timer.scheduleAtFixedRate(new ColorizeScreen(), 0, 100);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        timer.cancel();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        // register our heartBeatHandler with the DataLayerService. This ensures we get messages whenever the service receives something.
        DataLayerListenerService.setHeartBeatHandler(heartBeatHandler);
        DataLayerListenerService.setAnimationStateHandler(animationStateHandler);
    }

    @Override
    protected void onPause() {
        // unregister our heartBeatHandler so the service does not need to send its messages anywhere.
        DataLayerListenerService.setHeartBeatHandler(null);
        DataLayerListenerService.setAnimationStateHandler(null);
        super.onPause();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onStop() {
        if(animationService.isWearUsed()) {
            animationService.setToProgramPulseHistories();
            animationService.setStatusForDefaultLabels(lessonId, sizeOfPrograms, programList);
            animationService.defineLabel(lessonId);
        }
        super.onStop();  // Always call the superclass method first
    }

    private class ColorizeScreen extends TimerTask {

        @Override
        public void run() {
            if (!isStopped) {
                Canvas canvas = mSurfaceHolder.lockCanvas();

                Paint mClock = new Paint();
                mClock.setTextAlign(Paint.Align.CENTER);
                mClock.setTextSize(90);
                mClock.setColor(Color.parseColor("#FF4081"));

                if (programDuration == 0 && count == 1) {
                    curItem = 0;
                    canvas.drawColor(Color.WHITE);
                    canvas.drawText("Заново", widthScreen / 2, heightScreen - 30, mClock);
                    mSurfaceHolder.unlockCanvasAndPost(canvas);
                    timer.cancel();
                    return;
                } else if (programDuration == 0) {
                    isStart = true;
                    count--;
                    curPosition = "Вдох";
                    int i = sizeOfPrograms - count;

                    float p = userPulse;
                    if (curPulse != 0) {
                        p = curPulse;
                    }
                    float oneBit = 1000;

                    isSeconds = programList.get(i).getUnit();
                    if (!isSeconds) {
                        oneBit = (60000 / p);
                    }
                    curDuration = programList.get(i).getBreath() * oneBit;
                    programDuration = programList.get(i).getDuration() * oneBit;

                    mAngle = 0;
                    mPartAngle = 36000 / (float) curDuration;

                    curItem = 0;
                    startTimer = 3000;
                    startPosition = "Следующее";
                } else if (curItem == 3 && curDuration <=0) {
                    curPosition = "Вдох";
                    int i = sizeOfPrograms - count;

                    float p = userPulse;
                    if (curPulse != 0) {
                        p = curPulse;
                    }
                    float oneBit = 1000;
                    isSeconds = programList.get(i).getUnit();
                    if (!isSeconds) {
                        oneBit = (60000 / p);
                    }

                    curDuration = programList.get(i).getBreath() * oneBit;
                    curItem = 0;

                    mAngle = 0;
                    mPartAngle = 36000 / (float) curDuration;
                }

                try {
                    // draw
                    mPaint = new Paint();
                    mArcPaint = new Paint();
                    mArcPaint.setColor(CommonConstants.COLOR_SETTER.get(curPosition));//"#455B65"));//227, 232, 232));
                    mArcPaint.setColorFilter(new ColorFilter());
                    mOval = new RectF(-200, -400, widthScreen + 200, heightScreen + 400);
                    float oneBit = 1000;

                    canvas.drawColor(Color.WHITE);

                    if (startTimer != 0) {
                        mAngle = 0;
                        canvas.drawArc(mOval, 270, mAngle, true, mArcPaint);

                        int startTime = startTimer / 1000;
                        canvas.drawText(String.valueOf(startTime), widthScreen / 2, heightScreen / 2, mClock);
                        canvas.drawText(startPosition, widthScreen / 2, heightScreen - 30, mClock);
                        startTimer -= 100;
                        positionToWatch[0] = startPosition;
                        positionToWatch[1] = String.valueOf(startTime);
                        positionToWatch[3] = String.valueOf(count);
                        positionToWatch[4] = String.valueOf(sizeOfPrograms);
                        new SendToDataLayerThread(mGoogleApiClient, path, positionToWatch).start();
                    } else {
                        if (curDuration <= 0) {
                            float p = userPulse;
                            if (curPulse != 0) {
                                p = curPulse;
                            }
                            if (!isSeconds) {
                                oneBit = (60000 / p);
                            }
                            switch (curPosition) {
                                case "Вдох":
                                    curDuration = programList.get(sizeOfPrograms - count).getHoldFirst() * oneBit;
                                    curPosition = "Задержка";
                                    break;
                                case "Выдох":
                                    curDuration = programList.get(sizeOfPrograms - count).getHoldSecond() * oneBit;
                                    curPosition = "Задержка";
                                    break;
                                case "Задержка":
                                    curDuration = programList.get(sizeOfPrograms - count).getOut() * oneBit;
                                    curPosition = "Выдох";
                                    break;
                            }

                            curItem++;
                            mAngle = 0;
                            mPartAngle = 36000 / (float) curDuration;
                        }
                        canvas.drawArc(mOval, 270, mAngle += mPartAngle, true, mArcPaint);

                        positionToWatch[0] = curPosition;
                        positionToWatch[2] = String.valueOf(mAngle);
                        if (curDuration % 1000 > 0 && curDuration % 1000 <= 500) {
                            double curSec = (int) curDuration / 1000 + 0.5;
                            positionToWatch[1] = String.valueOf(curSec);
                            canvas.drawText(String.valueOf(curSec), widthScreen / 2, heightScreen / 2, mClock);
                        } else if (curDuration % 1000 > 500 && curDuration % 1000 <= 900) {
                            int curSec = (int) curDuration / 1000 + 1;
                            positionToWatch[1] = String.valueOf(curSec);
                            canvas.drawText(String.valueOf(curSec), widthScreen / 2, heightScreen / 2, mClock);
                        }

                        canvas.drawText(curPosition, widthScreen / 2, heightScreen / 2 + 160 , mClock);

                        canvas.drawText(String.valueOf((int) programDuration / oneBit), 120, heightScreen - 30, mClock);
                        //canvas.drawText(String.valueOf((int)lessonDuration/oneBit), widthScreen - 30, heightScreen - 30, mClock);
                        positionToWatch[3] = String.valueOf(count);
                        positionToWatch[4] = String.valueOf(sizeOfPrograms);
                        new SendToDataLayerThread(mGoogleApiClient, path, positionToWatch).start();

                        programDuration -= 100;
                        curDuration -= 100;
                        lessonDuration -= 100;
                    }

                } finally {
                    if (canvas != null) {
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
            else {
                positionToWatch[0] = "stop";
                positionToWatch[3] = String.valueOf(count);
                positionToWatch[4] = String.valueOf(sizeOfPrograms);
                new SendToDataLayerThread(mGoogleApiClient, path, positionToWatch).start();

            }
        }
    }
}