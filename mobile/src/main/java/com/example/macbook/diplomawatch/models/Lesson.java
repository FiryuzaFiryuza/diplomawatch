package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 13.11.15.
 */
public class Lesson extends PersistentObject{

    private String Name;

    private String Description;

    private double Duration;

    // 1 - системное; 0 - пользователем создано
    private boolean IsSystemLesson;

    public Lesson(long id, String name, String description, double duration, boolean isSystemLesson) {
        super(id);
        Name = name;
        Description = description;
        Duration = duration;
        IsSystemLesson = isSystemLesson;
    }

    public Lesson() {

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public double getDuration() {
        return Duration;
    }

    public void setDuration(double duration) {
        Duration = duration;
    }

    public boolean isSystemLesson() {
        return IsSystemLesson;
    }

    public void setIsSystemLesson(boolean isSystemLesson) {
        IsSystemLesson = isSystemLesson;
    }
}
