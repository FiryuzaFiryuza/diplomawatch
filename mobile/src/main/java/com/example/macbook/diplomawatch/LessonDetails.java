package com.example.macbook.diplomawatch;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.example.macbook.diplomawatch.adapters.ProgramSwipeAdapter;
import com.example.macbook.diplomawatch.animation.ColorizeActivity;
import com.example.macbook.diplomawatch.models.Lesson;
import com.example.macbook.diplomawatch.models.NameValuePair;
import com.example.macbook.diplomawatch.models.Program;
import com.example.macbook.diplomawatch.services.DBService;
import com.example.macbook.diplomawatch.services.NumberPickerService;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.marshalchen.ultimaterecyclerview.swipe.SwipeItemManagerInterface;

import java.util.ArrayList;

public class LessonDetails extends BaseActivity implements View.OnClickListener, NumberPicker.OnValueChangeListener {
    private EditText mLessonName;
    private EditText mLessonDescription;
    private Button mButtonStart;
    private FloatingActionButton mBttnEdit;
    private Button mBttnAddTags;

    private ArrayList<Program> mProgramList = new ArrayList<>();
    private ProgramSwipeAdapter adapter;
    private UltimateRecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private Lesson mLesson = new Lesson();
    private long lessonId;

    private DBService dbService = new DBService(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_details);

        initToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mLessonName = (EditText) findViewById(R.id.et_name_detail);
        mLessonDescription = (EditText) findViewById(R.id.et_description_detail);
        mButtonStart = (Button) findViewById(R.id.bttn_paly_lesson);
        mButtonStart.setOnClickListener(this);

        mBttnAddTags = (Button)findViewById(R.id.bttn_add_tags);
        mBttnAddTags.setOnClickListener(this);

        mBttnEdit = (FloatingActionButton) findViewById(R.id.bttn_add_new_program);
        mBttnEdit.setOnClickListener(this);

        Bundle id = getIntent().getExtras();
        lessonId = id.getLong("lesson");

        if(lessonId > 0) {
            init();
        }
        else {
            mBttnEdit.setVisibility(View.INVISIBLE);
        }

        /*LessonsListAdapter adapter = new LessonsListAdapter(mProgramList, this);
        mProgramListView = (ListView) findViewById(R.id.lv_programs);
        mProgramListView.setAdapter(adapter);*/

        mRecyclerView = (UltimateRecyclerView) findViewById(R.id.rv_programs);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new ProgramSwipeAdapter(mProgramList, this);
        adapter.setMode(SwipeItemManagerInterface.Mode.Single);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_save:
                save();
                return  true;
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_statistics:
                if(lessonId > 0) {
                    Intent intent = new Intent(LessonDetails.this, LessonChartsActivity.class);
                    intent.putExtra("lesson", lessonId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);
                }
                return true;
            case R.id.action_search:
                Intent intent = new Intent(LessonDetails.this, SearchExerciseActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.bttn_add_new_program:
                intent = new Intent(LessonDetails.this, ProgramDetails.class);
                intent.putExtra("new_program", lessonId);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.bttn_paly_lesson:
                show(lessonId);
                break;
            case R.id.bttn_add_tags:
                intent = new Intent(LessonDetails.this, AddSelectionsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("lesson", lessonId);
                intent.putExtra("program", (long)0);
                intent.putExtra("name", mLesson.getName());
                startActivity(intent);
                break;
        }
    }

    private void init() {
        mLesson = dbService.getLessonById(lessonId);
        mProgramList = dbService.getProgramsFromLesson(lessonId);

        mLessonName.setText(mLesson.getName());
        mLessonDescription.setText(mLesson.getDescription());
    }

    private void save()
    {
        mLesson.setName(String.valueOf(mLessonName.getEditableText()));
        mLesson.setDescription(String.valueOf(mLessonDescription.getEditableText()));

        if(lessonId > 0) {
            dbService.updateLesson(mLesson);
        }
        else {
            lessonId = dbService.addLesson(mLesson);
            mBttnEdit.setVisibility(View.VISIBLE);
        }
    }

    private void show(final long position)
    {
        NumberPickerService numberPickerService = new NumberPickerService(this, "Введите пульс", 0, 150);

        final Dialog d = numberPickerService.getmDialog();

        Button mBttnOk = numberPickerService.getmBttnOk();
        Button mBttnCancel = numberPickerService.getmBttnCancel();

        final NumberPicker mNumPicker = numberPickerService.getmNumberPicker();
        mNumPicker.setDisplayedValues(generateFloatNumbers());
        mNumPicker.setOnValueChangedListener(this);

        mBttnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double userPulse = (double) mNumPicker.getValue() + 50;

                ArrayList<NameValuePair> listForSend = new ArrayList<NameValuePair>();
                listForSend.add(new NameValuePair("lesson", String.valueOf(position)));
                listForSend.add(new NameValuePair("pulse", String.valueOf(userPulse)));

                Bundle extra = new Bundle();
                extra.putSerializable("objects", listForSend);

                Context context = v.getContext();
                Intent intent = new Intent(context, ColorizeActivity.class);
                intent.putExtra("start", extra);
                context.startActivity(intent);

                d.dismiss();
            }
        });
        mBttnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();
    }

    private String[] generateFloatNumbers() {
        String[] numbers = new String[251];
        for (int i = 0; i < 251; i ++) {
            numbers[i] = String.valueOf(i + 50);
        }

        return numbers;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }
}
