package com.example.macbook.diplomawatch.services;

import android.app.Dialog;
import android.content.Context;
import android.widget.Button;

import com.example.macbook.diplomawatch.R;
import java.util.ArrayList;

/**
 * Created by macbook on 27.12.15.
 */
public class NumberPickerService {

    private Dialog mDialog;
    private Button mBttnOk;
    private Button mBttnCancel;
    private android.widget.NumberPicker mNumberPicker;
    private Context context;
    private String title;
    private int minVal;
    private int maxVal;

    public NumberPickerService(Context context, String title, int minVal, int maxVal) {
        this.context = context;
        this.title = title;
        this.minVal = minVal;
        this.maxVal = maxVal;

        init();
    }

    private void init() {
        mDialog = new Dialog(context);
        mDialog.setTitle(title);
        mDialog.setContentView(R.layout.activity_number_picker_dialog);

        mBttnOk= (Button) mDialog.findViewById(R.id.button1);
        mBttnCancel = (Button) mDialog.findViewById(R.id.button2);

        mNumberPicker = (android.widget.NumberPicker) mDialog.findViewById(R.id.numberPicker1);
        mNumberPicker.setMaxValue(maxVal); // max value 100
        mNumberPicker.setMinValue(minVal);   // min value 0

        mNumberPicker.setWrapSelectorWheel(false);
        mNumberPicker.setDescendantFocusability(android.widget.NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mNumberPicker.setWrapSelectorWheel(false);
    }

    public Dialog getmDialog() {
        return mDialog;
    }

    public Button getmBttnOk() {
        return mBttnOk;
    }

    public Button getmBttnCancel() {
        return mBttnCancel;
    }

    public android.widget.NumberPicker getmNumberPicker() {
        return mNumberPicker;
    }
}
