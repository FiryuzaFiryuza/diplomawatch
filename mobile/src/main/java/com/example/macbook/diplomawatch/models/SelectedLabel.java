package com.example.macbook.diplomawatch.models;

/**
 * Created by macbook on 29.05.16.
 */
public class SelectedLabel extends PersistentObject {
    private String LabelName;
    private long LessonId;
    private long ProgramId;
    private String StartDateTime;
    private String EndDateTime;

    public SelectedLabel() {}

    public SelectedLabel(long lessonId, long programId, String s, String startDate, String endDate) {
        LessonId = lessonId;
        ProgramId = programId;
        LabelName = s;
        StartDateTime = startDate;
        EndDateTime = endDate;
    }

    public String getLabelName() {
        return LabelName;
    }

    public void setLabelName(String labelId) {
        LabelName = labelId;
    }

    public long getLessonId() {
        return LessonId;
    }

    public void setLessonId(long lessonId) {
        LessonId = lessonId;
    }

    public long getProgramId() {
        return ProgramId;
    }

    public void setProgramId(long programId) {
        ProgramId = programId;
    }

    public String getStartDateTime() {
        return StartDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        StartDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return EndDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        EndDateTime = endDateTime;
    }
}
