package com.example.macbook.diplomawatch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.macbook.diplomawatch.R;
import com.example.macbook.diplomawatch.models.Program;

import java.util.ArrayList;

/**
 * Created by macbook on 17.11.15.
 */
public class LessonsListAdapter extends BaseAdapter {
    private ArrayList<Program> mProgramList;
    private Context mContext;

    public class ViewHolder {
        TextView name;
        TextView duration;
        public int position;
    }

    private ViewHolder viewHolder;

    public LessonsListAdapter(ArrayList<Program> programList, Context context) {
        this.mProgramList = programList;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return this.mProgramList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.mProgramList.get(i);
    }

    public ArrayList<Program> getSightsList() {
        return this.mProgramList;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.programs_item, null);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.tv_program_name);
            viewHolder.duration = (TextView) convertView.findViewById(R.id.tv_program_duration);
            viewHolder.position = position;;
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.name.setText(this.mProgramList.get(position).getName());
        String unit = this.mProgramList.get(position).getUnit() ? "сек" : "кардио";
        viewHolder.duration.setText(String.valueOf(
                this.mProgramList.get(position).getDuration()) + " " +
                unit);

        convertView.setTag(viewHolder);

        return convertView;
    }
}
