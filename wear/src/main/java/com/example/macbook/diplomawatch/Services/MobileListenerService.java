package com.example.macbook.diplomawatch.services;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by macbook on 28.02.16.
 */
public class MobileListenerService extends WearableListenerService {
    private final String TAG = MobileListenerService.class.getSimpleName();
    private final String path = "/exercise/animation";

    private static Handler handler;

    public static Handler getHandler() {
        return handler;
    }

    public static void setHandler(Handler handler) {
        MobileListenerService.handler = handler;
        // send current value as initial value.
        if (handler != null){

        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        final String message = new String(messageEvent.getData());
        String[] params = message.split("-");
        if(handler!=null) {
            Message msg = handler.obtainMessage();
            msg.obj = params;
            handler.sendMessage(msg);
            Log.d(TAG, "Message received on watch is: " + message);
        }

        if (messageEvent.getPath().equals(path)) {
            Log.d(TAG, "Message received on watch is: " + message);
        } else {
            super.onMessageReceived(messageEvent);
        }
    }
}