package com.example.macbook.diplomawatch;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.wearable.view.WatchViewStub;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.macbook.diplomawatch.services.AnimationStateService;
import com.example.macbook.diplomawatch.services.HeartbeatService;
import com.example.macbook.diplomawatch.services.MobileListenerService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.util.HashMap;

public class MainActivity extends Activity implements View.OnClickListener, HeartbeatService.IOnChangeListener, SurfaceHolder.Callback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private static final String LOG_TAG = "MyHeart";
    private WatchViewStub mStub;
    private TextView mTextView;
    private TextView mTextViewPosition;
    private TextView mTextViewTime;

    private ImageView mPreviousBttn;
    private ImageView mNextBttn;

    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;
    private boolean canvasLocked;
    private boolean isStopped = false;

    private int heightScreen;
    private int widthScreen;

    private int count;
    private int sizeOfPrograms;

    private String path = "wear/exercise/animation";
    GoogleApiClient mGoogleApiClient;

    private ServiceConnection mobileServiceConnection;
    private ServiceConnection heartBeatServiceConnection;

    private Paint mPaint;
    private RectF mOval;
    private Paint mArcPaint;
    private float mAngle;
    private float mPartAngle;
    private HashMap<String, Integer> colorSetter = new HashMap<String, Integer>() {
        {
            put("Начинаем", Color.WHITE);
        }
        {
            put("Следующее", Color.WHITE);
        }
        {
            put("Заново", Color.WHITE);
        }
        {
            put("Вдох", Color.parseColor("#5CE5E8"));
        }

        {
            put("Задержка", Color.parseColor("#B5FF7D"));
        }

        {
            put("Выдох", Color.parseColor("#23A8E8"));
        }
    };

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String[] params = (String[]) msg.obj;
            if(params[0].equalsIgnoreCase("stop")) {
                isStopped = true;
                count = Integer.parseInt(params[3]);
                sizeOfPrograms = Integer.parseInt(params[4]);
                setControls();
            }
            else {
                //Canvas canvas = mSurfaceHolder.lockCanvas();
                Canvas canvas = null;
                try {
                    if (!canvasLocked) {
                        canvas = mSurfaceHolder.lockCanvas();
                        canvasLocked = true;

                        mTextViewPosition.setText(params[0]);
                        mTextViewTime.setText(params[1]);

                        mPaint = new Paint();
                        mArcPaint = new Paint();
                        mArcPaint.setColor(colorSetter.get(params[0]));//"#455B65"));//227, 232, 232));
                        mArcPaint.setColorFilter(new ColorFilter());
                        mOval = new RectF(-200, -400, widthScreen + 200, heightScreen + 400);

                        if(canvas != null) {
                            mSurfaceView.setBackgroundColor(Color.TRANSPARENT);
                            //if(canvas != )
                            canvas.drawColor(Color.WHITE);
                            if(isValid(params)) {
                                isStopped = false;
                                count = Integer.parseInt(params[3]);
                                sizeOfPrograms = Integer.parseInt(params[4]);
                                mAngle = Float.parseFloat(params[2]);
                                canvas.drawArc(mOval, 270, mAngle, true, mArcPaint);
                            }
                        }
                        else {
                            Log.d("TAG canvas", params[0] + "-" +params[1] + "-" + params[2]);
                        }
                    }
                } finally {
                    if (canvas != null) {
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                        canvasLocked = false;
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        heightScreen = metrics.heightPixels;
        widthScreen = metrics.widthPixels;

        initGoogleApiClient();
        initControls();
    }

    private void createSurfaceView(WatchViewStub stub) {
        mSurfaceView = (SurfaceView) stub.findViewById(R.id.sv_colorize);
        mSurfaceView.setBackgroundColor(Color.WHITE);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
    }

    private boolean isValid(String[] params) {
        return !params[0].equalsIgnoreCase("Начинаем") &&
                !params[0].equalsIgnoreCase("Следующее") &&
                !params[0].equalsIgnoreCase("Заново") &&
                !params[0].equalsIgnoreCase("stop") &&
                !params[2].equalsIgnoreCase("null") &&
                !params[3].equalsIgnoreCase("null") &&
                !params[4].equalsIgnoreCase("null");
    }

    private void initControls() {
        mStub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        // inflate layout depending on watch type (round or square)

        mobileServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder binder) {
                Log.d(LOG_TAG, "connected to service.");
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };

        heartBeatServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder binder) {
                Log.d(LOG_TAG, "connected to service.");
                // set our change listener to get change events
                ((HeartbeatService.HeartbeatServiceBinder) binder).setChangeListener(MainActivity.this);

            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };

        mStub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                // as soon as layout is there...
                mTextViewPosition = (TextView) stub.findViewById(R.id.tv_cur_position);
                mTextViewTime = (TextView) stub.findViewById(R.id.tv_cur_time);
                mTextView = (TextView) stub.findViewById(R.id.heartbeat);

                mNextBttn = (ImageView) stub.findViewById(R.id.bttn_next);
                mPreviousBttn = (ImageView) stub.findViewById(R.id.bttn_previous);

                createSurfaceView(stub);

                // bind to our service.
                bindService(new Intent(MainActivity.this, HeartbeatService.class), heartBeatServiceConnection, Service.BIND_AUTO_CREATE);

                bindService(new Intent(MainActivity.this, MobileListenerService.class), mobileServiceConnection, Service.BIND_AUTO_CREATE);

            }
        });

        mStub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStopped = !isStopped;
                if (isStopped) {
                    Log.d("View Click", "click");
                    new AnimationStateService(mGoogleApiClient, path, "stop").start();

                    if (count > 1) {
                        Log.d("View Click next", "click");
                        mNextBttn.setVisibility(View.VISIBLE);
                    }

                    if (sizeOfPrograms - count > 0) {
                        Log.d("View Click previous", "click");
                        mPreviousBttn.setVisibility(View.VISIBLE);
                    }

                } else {
                    new AnimationStateService(mGoogleApiClient, path, "play").start();

                    mNextBttn.setVisibility(View.INVISIBLE);
                    mPreviousBttn.setVisibility(View.INVISIBLE);
                }
            }
        });
    }


    private void setControls() {
        if (isStopped) {
            if (count > 1)
                mNextBttn.setVisibility(View.VISIBLE);
            else
                mNextBttn.setVisibility(View.INVISIBLE);

            if (sizeOfPrograms - count > 0)
                mPreviousBttn.setVisibility(View.VISIBLE);
            else
                mPreviousBttn.setVisibility(View.INVISIBLE);

        } else {
            mNextBttn.setVisibility(View.INVISIBLE);
            mPreviousBttn.setVisibility(View.INVISIBLE);
        }
    }



    private void initGoogleApiClient() {
        if (null == mGoogleApiClient) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            Log.v("TAG", "GoogleApiClient created");
        }

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            Log.v("TAG", "Connecting to GoogleApiClient..");
        }

        startService(new Intent(this, MobileListenerService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobileListenerService.setHandler(handler);
    }

    @Override
    public void onValueChanged(int newValue) {
        // will be called by the service whenever the heartbeat value changes.
        mTextView.setText(Integer.toString(newValue));
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mAngle = 0;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        //unbindService(mobileServiceConnection);
        //unbindService(heartBeatServiceConnection);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.bttn_next:
                mNextBttn.setVisibility(View.INVISIBLE);
                new AnimationStateService(mGoogleApiClient, path, "next").start();
            case R.id.bttn_previous:
                mPreviousBttn.setVisibility(View.INVISIBLE);
                new AnimationStateService(mGoogleApiClient, path, "previous").start();
        }
    }

    public void onPrevClick(View v) {
        mPreviousBttn.setVisibility(View.INVISIBLE);
        new AnimationStateService(mGoogleApiClient, path, "previous").start();
    }

    public void onNextClick(View v) {
        mNextBttn.setVisibility(View.INVISIBLE);
        new AnimationStateService(mGoogleApiClient, path, "next").start();
    }

}
