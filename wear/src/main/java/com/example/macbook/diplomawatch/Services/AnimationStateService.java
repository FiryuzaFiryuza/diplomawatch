package com.example.macbook.diplomawatch.services;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by macbook on 03.04.16.
 */
public class AnimationStateService extends Thread {

    String path;
    String message;
    GoogleApiClient mGoogleApiClient;

    // Constructor to send a message to the data layer
    public AnimationStateService(GoogleApiClient googleApiClient, String p, String msg) {
        path = p;
        message = msg;
        mGoogleApiClient = googleApiClient;
    }

    public void run() {
        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : nodes.getNodes()) {
            MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleApiClient, node.getId(), path, message.getBytes()).await();
            if (result.getStatus().isSuccess()) {
                Log.d("TAG", "Message: {" + message + "} sent to: " + node.getDisplayName());
            } else {
                // Log an error
                Log.d("TAG", "ERROR: failed to send Message");
            }
        }
    }
}
